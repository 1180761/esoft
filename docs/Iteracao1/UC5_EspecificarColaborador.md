# UC5 - Especificar Colaborador de Organização

## 1. Engenharia de Requisitos

### Formato Breve

O gestor de organização inicia a especificação de um novo colaborador de organização. O sistema solicita dados (i.e. nome, email, função, telemóvel, password). O gestor de organização introduz os dados solicitados. O sistema apresenta os dados e pede confirmação. O gestor de organização confirma. O sistema regista o novo colaborador da organização, informando o gestor de organização do sucesso da operação.		

### SSD
![UC5_SSD.svg](UC5_SSD.svg)


### Formato Completo

#### Ator principal

Gestor de Organização

#### Partes interessadas e seus interesses
* **Gestor de Organização:** pretende iniciar um novo colaborador da sua organização para que este possa usufruir das funcionalidades da plataforma.
* **T4J:** pretende que o gestor da organização registe um colaborador da organização de modo a que este possa utilizar a plataforma.


#### Pré-condições
n/a

#### Pós-condições
A informação do registo é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Gestor de Organização inicia o registo de um colaborador de organização. 
2. O sistema solicita os dados necessários sobre o colaborador (i.e. nome, email, função, telemóvel, password)
3. O Gestor de Organização introduz os dados solicitados. 
4. O sistema valida e apresenta os dados, pedindo que os confirme. 
5. O Gestor de Organização confirma. 
6. O sistema **regista um novo colaborador de organização no sistema, e informa o gestor de organização do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O gestor de organização solicita o cancelamento do registo.

> O caso de uso termina.
	
4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.

4b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

>	1.O sistema alerta o colaborador de organização para o facto.
>	2.O sistema permite a sua alteração (passo 3).
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina. 
 
4c. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o gestor de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados obrigatórios para além dos já conhecidos?
* Quais os dados que em conjunto permitem detetar a duplicação de colaboradores?
* A password é gerada aleatóriamente aquando do registo?
* Quais são as regras de segurança aplicaveis à palavra-passe?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para a UC

![UC5_MD.svg](UC5_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O gestor de organização inicia o registo de um novo colaborador da organização   		 |	... interage com o utilizador? | EspecificarColaboradorUI   |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| EspecificarColaboradorController | Controller (Modelo GRASP)    |
|  		 |	... cria instância de Colaborador | Organizacao  | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários sobre o colaborador (i.e. nome, email, função, telemóvel, password)		 |							 |             |                              |
| 3. O Gestor de Organização introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   Colaborador | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema valida e apresenta os dados, pedindo que os confirme.   		 |	...valida os dados do Colaborador (validação local) | Colaborador | IE. Possui os seu próprios dados.|  	
|																	  		 | ...valida os dados do Colaborador (validação global) | Organizacao | A organização contém/agrega o Colaborador. |
| 5. O Gestor de Organização confirma.   		 |							 |             |                              |
| 6. O sistema regista um novo colaborador de organização no sistema, e informa o gestor de organização do sucesso da operação.  		 |	... guarda a Colaborador criada? | Organização  | IE: No MD a Organização possui Colaborador| 
|																																		 | ...notifica o utilizador? | EspecificarColaboradorUI. |
             

### Diagrama de Sequência

![UC5_SD.svg](UC5_SD.svg)

### Diagrama de Classes

![UC5_CD.svg](UC5_CD.svg)
