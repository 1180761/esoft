﻿﻿# UC4 - Especificar Competência Técnica

## 1. Engenharia de Requisitos

### Formato Breve
O administrativo inicia a especificação de uma nova competência técnica. O sistema solicita os dados necessários (i.e. código único, descrição breve e detalhada). O administrativo introduz os dados solicitados. O sistema apresenta as áreas de atividade. O administrativo seleciona a área de atividade referente. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC4_SSD.svg](UC4_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende especificar competências técnicas requeridas para categorias de tarefas.
* **T4J:** pretende que a plataforma permita especificar competências técnicas requeridas para categorias de tarefas.


#### Pré-condições
n/a

#### Pós-condições
A informação da competência técnica é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a especificação de uma nova competência técnica. 
2. O sistema solicita os dados necessários (i.e. código único, descrição breve e detalhada). 
3. O administrativo introduz os dados solicitados.
4. O sistema apresenta as áreas de atividade e solicita a seleção da área de atividade referente.
5. O administrativo seleciona a área de atividade referente.
6. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
7. O administrativo confirma. 
8. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da especificação de uma nova competência técnica.

> O caso de uso termina.

4a. Não existem áreas de atividade no sistema.
> 	1. O sistema informa o administrativo de tal facto.
>	2. O sistema permite definição de uma nova área de atividade (UC2 - Definir Área de Atividade).
>	
	>	2a. O administrativo não define uma nova área de atividade. O caso de uso termina.


5a. O administrativo informa que pretende usar uma outra área de atividade.
>	1. O sistema permite definição de uma nova área de atividade (UC2 - Definir Área de Atividade).
>	2. O sistema volta para o passo 4.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3).
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3).
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

6c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o administrativo para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 
	> 	2a. O administrativo não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Qual a frequência de ocorrência deste caso de uso?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC4_MD.svg](UC4_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a especificação de uma nova competência técnica.| ... interage com o utilizador?| DefinirCompetenciaTecnicaUI| 	Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
||...coordena o UC?|DefinirCompetenciaTecnicaController| Controller |
||...cria/instancia CompetenciaTecnica?|Plataforma| Creator (Regra1)|
| 2. O sistema solicita os dados necessários (i.e. código único, descrição breve e detalhada). | | | |
| 3. O administrativo introduz os dados solicitados. | ... guarda os dados introduzidos?| CompetenciaTecnica | Information Expert (IE) - instância criada no passo 1|
| 4. O sistema mostra a lista de áreas de atividade para que seja selecionada uma.| ...conhece as áreas de atividade existentes a listar?| Plataforma| IE: Plataforma tem/agrega todas as áreas de atividade|
| 5. O administrativo seleciona a área de atividade em que se enquadra.| ... guarda a área de atividade selecionada?| CompetenciaTecnica| IE: CompetenciaTecnica refere uma área de atividade - instância criada anteriormente|
| 6. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.| ...valida os dados da CompetenciaTecnica (validação local)?| CompetenciaTecnica| IE: CompetenciaTecnica possui os seus próprios dados|
| | ...valida os dados da CompetenciaTecnica (validação global)?| Plataforma | IE: A Plataforma contém/agrega CompetenciaTecnica|| 
| 7. O administrativo confirma.| | | |
| 8. O sistema regista os dados e informa o administrativo do sucesso da operação.| ...guarda a CompetenciaTecnica especificado/criado?| Plataforma | IE. No MD a Plataforma contém/agrega CompetenciaTecnica|
|| ... notifica o utilizador?                                                                                   | DefinirCompetenciaTecnicaUI                                        |                                                |                                                                                                                      

             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CompetenciaTecnica
 * AreaAtividade


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCompetenciaTecnicaUI 
 * DefinirCompetenciaTecnicaController


###	Diagrama de Sequência

![UC4_SD.svg](UC4_SD.svg)


###	Diagrama de Classes

![UC4_CD.svg](UC4_CD.svg)