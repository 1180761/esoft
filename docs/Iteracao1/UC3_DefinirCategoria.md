﻿﻿﻿# UC3 - Definir Categoria (de Tarefa)

## 1. Engenharia de Requisitos

### Formato Breve
O administrativo inicia a definição de uma nova categoria. O sistema solicita os dados necessários (i.e. descrição). O administrativo introduz os dados solicitados. O sistema apresenta as áreas de atividade e solicita uma. O administrativo seleciona uma. O sistema apresenta as competências técnicas e solicita competência(s) técnica(s). O administrativo seleciona competência(s) técnica(s). O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende definir as categorias para que possa posteriormente catalogar as tarefas.
* **T4J:** pretende que a plataforma permita catalogar as tarefas em categorias.


#### Pré-condições
n/a

#### Pós-condições
A informação da categoria é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma nova categoria. 
2. O sistema solicita os dados necessários (i.e. descrição). 
3. O administrativo introduz os dados solicitados.
4. O sistema apresenta as áreas de atividade e solicita uma.
5. O administrativo seleciona a área de atividade em que se enquadra.
6. O sistema apresenta as competências técnicas e solicita uma.
7. O administrativo seleciona uma competência técnica.
8. Os passos 6 e 7 repetem-se enquanto não forem introduzidos todas as competências técnicas pretendidas (minimo 1).
9. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
10. O administrativo confirma. 
11. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da categoria.

> O caso de uso termina.

4a. Não existem áreas de atividade no sistema.
> 	1. O sistema informa o administrativo de tal facto.
>	2. O sistema permite a definição de uma nova área de atividade (UC2 - Definir Área de Atividade).
>	
	>	2a. O administrativo não define uma nova área de atividade. O caso de uso termina.

5a. O administrativo informa que pretende usar uma outra área de atividade.
>	1. O sistema permite definição de uma nova área de atividade (UC2 - Definir Área de Atividade).
>	2. O sistema volta para o passo 4.

6a. Não existem competências técnicas no sistema.
> 	1. O sistema informa o administrativo de tal facto.
>	2. O sistema permite a especificação de uma nova competência técnica (UC4 - Especificar Competência Técnica).
>	
	>	2a. O administrativo não define uma nova competência técnica. O caso de uso termina.

7a. O administrativo informa que pretende usar uma outra competência técnica.
>	1. O sistema permite especificação de uma nova competência técnica (UC4 - Especificar Competência Técnica).
>	2. O sistema volta para o passo 6.

9a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3).
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

9b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3).
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

9c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o administrativo para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 
	> 	2a. O administrativo não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Qual a frequência de ocorrência deste caso de uso?
* Quais as competências que têm caracter obrigatório e quais as desejáveis?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC3_MD.svg](UC3_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a especificação de uma nova categoria.| ... interage com o utilizador?| DefinirCategoriaUI| 	Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
||...coordena o UC?|DefinirCategoriaController| Controller |
||...cria/instancia Categoria?|Plataforma| Creator (Regra1)|
| 2. O sistema solicita os dados necessários (i.e. descrição). | | | |
| 3. O administrativo introduz os dados solicitados. | ... guarda os dados introduzidos?| Categoria | Information Expert (IE) - instância criada no passo 1|
| 4. O sistema mostra a lista de áreas de atividade para que seja selecionada uma.| ...conhece as áreas de atividade existentes a listar?| Plataforma| IE: Plataforma tem/agrega todas as áreas de atividade|
| 5. O administrativo seleciona a área de atividade em que se enquadra.| ... guarda a área de atividade selecionada?| Categoria| IE: Categoria enquadra-se numa área de atividade - instância criada anteriormente|
| 6. O sistema mostra a lista de competências técnicas para que seja selecionada uma.| ...conhece as competências técnicas existentes a listar?| Plataforma| IE: Plataforma tem/agrega todas as competências técnicas|
| 7. O administrativo seleciona a competência técnica requerida.| ... guarda a competência técnica selecionada?| Categoria| IE: Categoria requer competência(s) técnica(s) - instância criada anteriormente|
| 8. Os passos 6 e 7 repetem-se enquanto não forem introduzidos todas as competências técnicas pretendidas (minimo 1). ||||
| 9. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.| ...valida os dados da Categoria (validação local)?| Categoria| IE: Categoria possui os seus próprios dados|
| | ...valida os dados da Categoria (validação global)?| Plataforma | IE: A Plataforma contém/agrega Categoria|| 
| 10. O administrativo confirma.| | | |
| 11. O sistema regista os dados e informa o administrativo do sucesso da operação.| ...guarda a Categoria especificada/criada?| Plataforma | IE. No MD a Plataforma contém/agrega Categoria|
|| ... notifica o utilizador?                                                                                   | DefinirCategoriaUI                                        |                                                |                                                                                                                      


             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Categoria
 * CompetenciaTecnica
 * AreaAtividade


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaUI  
 * DefinirCategoriaController


###	Diagrama de Sequência

![UC3_SD.svg](UC3_SD.svg)


###	Diagrama de Classes

![UC3_CD.svg](UC3_CD.svg)