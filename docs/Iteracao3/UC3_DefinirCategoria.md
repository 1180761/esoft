﻿﻿﻿# UC3 - Definir Categoria (de Tarefa)

## 1. Engenharia de Requisitos

### Formato Breve
O administrativo inicia a definição de uma nova categoria. O sistema solicita os dados necessários (i.e. descrição). O administrativo introduz os dados solicitados. O sistema apresenta as áreas de atividade e solicita uma. O administrativo seleciona uma. O sistema apresenta as competências técnicas e solicita competência(s) técnica(s) . O administrativo seleciona competência(s) técnica(s). O sistema mostra os graus de proficiência aplicáveis a cada competência técnica e pede para selecionar o grau mínimo exigível bem como o seu carácter. O administrador seleciona o grau pretendido e introduz o carácter de cada competência técnica. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende definir as categorias para que possa posteriormente catalogar as tarefas.
* **T4J:** pretende que a plataforma permita catalogar as tarefas em categorias.


#### Pré-condições
n/a

#### Pós-condições
A informação da categoria é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma nova categoria. 
2. O sistema solicita os dados necessários (i.e. descrição). 
3. O administrativo introduz os dados solicitados.
4. O sistema apresenta as áreas de atividade e solicita uma.
5. O administrativo seleciona a área de atividade em que se enquadra.
6. O sistema mostra a lista de competências técnicas referentes à área de atividade previamente selecionada.
7. O administrativo seleciona uma competência técnica.
8. O sistema mostra os graus de proficiência aplicáveis a essa competência técnica e pede para selecionar o grau mínimo exigível bem como o seu carácter (i.e. obrigatória ou desejável).
9. O administrador seleciona o grau pretendido e introduz carácter da competência técnica.
10. Os passos 6 a 9 repetem-se enquanto não forem introduzidas todos os competências técnicas pretendidas.
11. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
12. O administrativo confirma. 
13. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da categoria.

> O caso de uso termina.

4a. O sistema deteta que a lista de áreas de atividades está vazia.
>		1. O sistema informa o administrativo de tal facto.  
>		2. O sistema permite a definição de uma nova área de atividade (UC2).  
>		2a. O administrativo não define uma área de atividade. O caso de uso termina.

5a. O administrativo não encontra a área de atividade pretendida.
>		1. O administrativo informa o sistema de tal facto.  
>		2. O sistema permite a definição de uma nova área de atividade (UC2).  
>		2a. O administrativo não define uma área de atividade. O caso de uso termina.

6a. O sistema deteta que a lista de competências técnicas está vazia.

>		1. O sistema informa o administrativo de tal facto.  
>		2. O sistema permite a especificação de uma nova competência técnica (UC4).  
>		2a. O administrativo não especifica uma competência técnica. O caso de uso termina.

7a. O administrativo não encontra a competência técnica pretendida.
>		1. O administrativo informa o sistema de tal facto.  
>		2. O sistema permite a especificação de uma nova competência técnica (UC4).  
>		2a. O administrativo não especifica uma competência técnica. O caso de uso termina.

11a. Dados mínimos obrigatórios em falta.

>		1. O sistema informa quais os dados em falta.
>		2. O sistema permite a introdução dos dados em falta (passo 3).
>		
>		2a. O administrativo não altera os dados. O caso de uso termina.

11b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>		1. O sistema alerta o administrativo para o facto.
>		2. O sistema permite a sua alteração (passo 3).
>		
>		2a. O administrativo não altera os dados. O caso de uso termina.

11c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
> 2a. O administrativo não altera os dados. O caso de uso termina.

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Qual a frequência de ocorrência deste caso de uso?
* Quais as competências que têm caracter obrigatório e quais as desejáveis?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC3_MD.svg](UC3_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal                                              | Questão: Que Classe...                                       | Resposta                    | Justificação                                                 |
| :----------------------------------------------------------- | :----------------------------------------------------------- | :-------------------------- | :----------------------------------------------------------- |
| 1. O administrativo inicia a definição de uma nova categoria de tarefa. | ... interage com o utilizador?                               | DefinirCategoriaUI          | Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
|                                                              | ... coordena o UC?                                           | DefinirCategoriaController  | Controller                                                   |
|                                                              | ... cria instância de Categoria?                             | RegistoCategorias           | Creator (regra 1) + HC +LC sobre Plataforma                  |
|                                                              | ...gera o identificador único da Categoria?                  | RegistoCategorias           | IE: o RegistoCategorias conhece todas as Categorias.         |
| 2. O sistema solicita a descrição.                           |                                                              |                             |                                                              |
| 3. O administrativo introduz a descrição.                    | ... guarda os dados introduzidos?                            | Categoria                   | Information Expert (IE) - instância criada no passo 1.       |
| 4. O sistema mostra a lista de áreas de atividade para que seja selecionada uma. | ...conhece as áreas de atividades?                           | RegistoAreasAtividade       | IE: RegistoAreasAtividade possui AreaAtividade               |
| 5. O administrativo seleciona uma área de atividade.         | ... guarda a área selecionada?                               | Categoria                   | IE: instância criada no passo 1. No MD uma Categoria é referente a uma AreaAtividade. |
| 6. O sistema mostra a lista de competências técnicas referentes à área de atividade previamente selecionada para que seja selecionada uma. | .. conhece as competências técnicas?                         | RegistoCompetenciasTecnicas | IE: RegistoCompetenciasTecnicas possui CompetenciaTecnica    |
|                                                              | ...sabe a que área de atividade a competência técnica se refere? | CompetenciaTecnica          | IE: cada CompetenciaTecnica conhece a AreaAtividade em que se enquadra. |
| 7. O administrativo escolhe uma competência técnica da lista. | ... guarda a competência técnica selecionada?                | CaraterCT                   | IE: no MD cada Categoria tem várias CaraterCT sendo cada uma referente a uma CompetenciaTecnica (CT). |
|                                                              | ...cria instância de CaracterCT                              | Categoria                   | IE: no MD cada Categoria tem várias CaraterCT.               |
| 8. O sistema mostra os graus de proficiência aplicáveis a essa competência técnica e pede para selecionar o grau mínimo exigível bem como o seu carácter (i.e. obrigatória ou desejável). | .. conhece os graus de proficiência?                         | CompetenciaTecnica          | IE: no MD cada CompetenciaTecnica tem vários GrauProficiencia. |
| 9. O administrador seleciona o grau pretendido e introduz carácter da competência técnica. | ... guarda a informação introduzida?                         | CaraterCT                   | IE. instância criada no passo 7, no MD cada CaraterCT tem um GrauProficiencia sendo cada um referente a uma CompetenciaTecnica (CT). |
| 10. Os passos 7 a 9 repetem-se enquanto não forem introduzidas todas as competências técnicas pretendidas. |                                                              |                             |                                                              |
| 11. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. | ...valida os dados da Categoria (validação local)            | Categoria                   | IE. A Categoria possui os seus próprios dados.               |
|                                                              | ...valida os dados da Categoria (validação global)           | RegistoCategorias           | IE: O RegistoCategorias possui/agrega Categoria.             |
| 12. O administrativo confirma.                               |                                                              |                             |                                                              |
| 13. O sistema regista os dados e informa o administrativo do sucesso da operação. | ... guarda a Categoria de tarefa criada?                     | RegistoCategorias           | IE: O RegistoCategorias possui/agrega Categoria.             |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Categoria
 * CompetenciaTecnica
 * AreaAtividade
 * CaracterCT
 * GrauProficiencia


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaUI  
 * DefinirCategoriaController
 * RegistoAreasAtividade
 * RegistoCompetenciasTecnicas
 * RegistoCategorias


###	Diagrama de Sequência

![UC3_SD.svg](UC3_SD.svg)


###	Diagrama de Classes

![UC3_CD.svg](UC3_CD.svg)