﻿# UC 10 - Seriar (Não Automaticamente) Candidaturas de Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia o processo não automático de seriação dos candidatos à realização de um anúncio. O sistema mostra os anúncios publicados pelo colaborador em fase de seriação não automática e que ainda não foram seriadas e pede-lhe para escolher um. O colaborador seleciona um anúncio. O sistema mostra as candidaturas existente e solicita a sua classificação e a respetiva justificação. O colaborador classifica as candidaturas. O sistema mostra os colaboradores da organização e pede para selecionar os outros participantes no processo. O colaborador seleciona outros colaboradores. O sistema solicita um texto de conclusão. O colaborador introduz um texto de conclusão. O sistema valida e apresenta os dados, pedindo que os confirme. O colaborador confirma. O sistema regista os dados juntamente com a data/hora atual e informa o colaborador do sucesso da operação.

### SSD
![UC10-SSD.svg](UC10-SSD.svg)

### Formato Completo

#### Ator principal

* Colaborador de Organização

#### Partes interessadas e seus interesses

* **Colaborador de Organização:** pretende seriar as candidaturas que um anúncio recebeu.
* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições

* Existir pelo menos um anúncio de tarefa em condições de ser seriado manualmente pelo colaborador ativo no sistema.

#### Pós-condições
* A informação do processo de seriação é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador inicia o processo não automático de seriação das candidaturas a um anúncio.
2. O sistema mostra os anúncios publicadas pelo colaborador em fase de seriação não automática e que ainda não foram seriados e pede-lhe para escolher um.
3. O colaborador seleciona um anúncio.
4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas.
5. O colaborador seleciona uma candidatura.
6. O sistema solicita a classificação da candidatura selecionada.
7. O colaborador indica a classificação e a respetiva justificação.
8. Os passos 4 a 7 repetem-se até que estejam classificadas todas as candidaturas.
9. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.
10. O colaborador seleciona um colaborador.
11. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.
12. O sistema solicita um texto de conclusão. 
13. O colaborador introduz um texto de conclusão.
14. O sistema valida e apresenta os dados, pedindo ao colaborador para confirmar.
15. O colaborador confirma.
16. O sistema regista os dados juntamente com a data/hora atual e informa o colaborador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento do processo de seriação das candidaturas.
> O caso de uso termina.

14a. Dados mínimos obrigatórios em falta.

>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 2)
>
	> 2a. O colaborador não altera os dados. O caso de uso termina.



#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Há algum motivo que possa levar à desclassificação de uma candidatura? 
* Todos os dados são obrigatórios?
* É necessário o sistema validar todos os dados introduzidos?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10-MD.svg](UC10-MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal                                              | Questão: Que Classe...                                 | Resposta                | Justificação                                                 |
| :----------------------------------------------------------- | :----------------------------------------------------- | :---------------------- | :----------------------------------------------------------- |
| 1. O colaborador de organização inicia a seriação de um anuncio. | ... interage com o utilizador?                         | SeriarAnuncioUI         | Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|                                                              | ...coordena o UC?                                      | SeriarAnuncioController | Controller                                                   |
|                                                              | ...cria/instancia ProcessoSeriacao?     | Anuncio | Creator (regra 1) + HC +LC sobre Plataforma                  |
| 2. O sistema apresenta a lista de anúncios publicados pelo colaborador e solicita um. | ...conhece os anuncios a listar?                       | RegistoAnuncios         | IE: RegistoAnuncios tem/agrega todas os anuncios             |
| 3. O colaborador de organização seleciona um anuncio.        | ... guarda o anuncio?                                  | ProcessoSeriacao          | Information Expert (IE) - instância criada no passo 1,no MD cada SeriarAnuncio tem um anuncio |
| 4. O sistema apresenta a lista de candidaturas e solicita a classificação de cada uma. | ...conhece as candidaturas existentes a listar?        | Anuncio                 | IE: Anuncio tem/agrega candidaturas                          |
| 5. O colaborador de organização classifica uma candidatura.  | ... guarda a Classificacao?                           | ProcessoSeriacao | IE: ProcessoSeriacao tem Classificacao |
|                                                              | ...cria instância de Classificacao?                              | ProcessoSeriacao | IE: no MD cada ProcessoSeriacao tem várias Classificacao.               |
| 6. Os passos 4 a 5 repetem-se enquanto não forem classificadas todas as candidaturas. |                                                        |                         |                                                              |
| 7. O sistema apresenta a lista de colaboradores e solicita a escolha de um participante. | ...conhece os colaboradores existentes a listar?       | Organizacao             | IE: Organizacao tem/agrega colaboradores                     |
| 8. O colaborador de organização seleciona um participante.   | guarda o colaborador?                                  | ProcessoSeriacao | IE: ProcessoSeriacao tem/agrega colaboradores   |
| 9. Os passos 7 a 8 repetem-se enquanto não forem introduzidas todos os participantes. |                                                        |                         |                                                              |
| 10. O sistema solicita uma conclusão. |                                                        |                         |                                                              |
| 11. O colaborador de organização introduz os dados solicitados. | guarda os dados introduzidos?                          | ProcessoSeriacao |                                                              |
| 12. O sistema mostra o melhor candidato e pede confirmação.   | ...conhece o candidato?                                      | Candidatura                       | IE: Candidatura tem um freelancer associado                  |
| 13. O Colaborador de Organização confirma o candidato.             | ... guarda o candidato selecionado?                          | ProcessoAtribuicaoAdjudicacao     | IE: ProcessoAtribuicaoAdjudicacao                            |
| 14. O sistema apresenta os dados da adjudicação (i.e. organização, freelancer, descrição da tarefa, período da tarefa, valor aceite por ambas as partes, referência ao anúncio que lhe deu origem, número sequencial por ano, data da adjudicação) e pede confirmação. | ...valida os dados do ProcessoAtribuicaoAdjudicacao (validação local)? | ProcessoAtribuicaoAdjudicacao     | IE: ProcessoAtribuicaoAdjudicacao  possui os seus próprios dados |
|                                                              | ...valida os dados da ProcessoAtribuicaoAdjudicacao (validação global)? | RegistoAdjudicacao                | IE: O RegistoProcessoAtribuicaoAdjudicacao contém/agrega ProcessoAtribuicaoAdjudicacao |
|                                                              | ...valida os dados da ProcessoAtribuicaoAdjudicacao (validação global)? | Anuncio                           | IE: O Anuncio contém/agrega ProcessoAtribuicaoAdjudicacao    |
|                                                              | ...sabe a referência do anúncio?                             | Anúncio                           | O Anúncio contém a referência única da tarefa                |
|                                                              | ...sabe a descrição da tarefa?                               | Tarefa                            | O anúncio contém a referência da tarefa                      |
|                                                              | ...sabe a duração da tarefa?                                 | Tarefa                            | O anúncio contém a referência da tarefa                      |
|                                                              | ...cria o número sequencial (por ano)?                       | Interface GeradorNumeroSequencial | O GeradorNumeroSequencial gera um número sequencial único para a adjudicação |
|                                                              | ...sabe a data atual?                                        | Plataforma                        | A plataforma                                                 |
|                                                              | ...sabe o valor aceite por ambas as partes?                  | Candidatura                       | A Candidatura sabe o valor concordante por ambas as partes   |
| 15. O Colaborador de organização confirma.                   | ...guarda o ProcessoAtribuicaoAdjudicacao especificado/criado? | Anuncio                           | IE. O Anuncio contém/agrega ProcessoAtribuicaoAdjudicacao    |
| 16. Os passos 12 a 16 são executados se o colaborador pretender. ||||
| 17. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. | ...valida os dados do ProcessoSeriacao (validação local) | ProcessoSeriacao | IE. A ProcessoSeriacaopossui os seus próprios dados. |
|                                                              | ...valida os dados da ProcessoSeriacao (validação global) | Anuncio | IE: O Anuncio possui/agrega ProcessoSeriacao. |
| 18. O colaborador de organização confirma.                   |                                                        |                         |                                                              |
| 19. O sistema regista os dados juntamente com a data/hora atual e informa o colaborador de organização do sucesso da operação. | ... guarda o ProcessoSeriacao criado?    | Anuncio | IE: O Anuncio  possui/agrega ProcessoSeriacao.  |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * ProcessoSeriacao
 * TipoRegimento
 * Classificacao
 * Organizacao
 * Colaborador
 * Tarefa
 * ProcessoAtribuicaoAdjudicacao
 * GeradorNumeroSequencial

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController
 * RegistoAnuncios
 * RegistoOrganizacoes
 * RegistoProcessoAtribuicaoAdjudicacao
 * ListaColaboradores
 * ListaCandidaturas
 * ListaTarefas
 * ListaClassificacao


###	Diagrama de Sequência

![UC10-SD.svg](UC10-SD.svg)

![UC10-SD2.svg](UC10-SD2.svg)

###	Diagrama de Classes

![UC10-CD.svg](UC10-CD.svg)
