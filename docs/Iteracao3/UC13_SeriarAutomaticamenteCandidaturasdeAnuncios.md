# UC13 - Seriar (automaticamente) Candidaturas de Anúncios

## 1. Engenharia de Requisitos

### Formato Breve

O Timer inicializa a seriação automatica das candidaturas quando o horário definido é atingido.

### SSD
![UC13_SSD.svg](UC13_SSD.svg)


### Formato Completo

#### Ator principal

Timer

#### Partes interessadas e seus interesses

* **T4J:** Pretende que as candidaturas sejam seriadas automaticamente quando o tipo de regimento do anuncio assenta em critérios objetivos e automáticos.

#### Pré-condições
Existência de Anuncios no sistema, candidatos a esses anuncios e um tipo de regimento que assente em critérios objetivos

#### Pós-condições
A informação da seriação é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O horário de seriação é atingido.
2. O sistema identifica os anuncios cujo tipo de regimento estipula critérios objetivos, que estejam no periodo de seriação e que que ainda não tenham sido seriados, efetua a seriação e de seguida a sua atribuição, caso seja obrigatória.

#### Extensões (ou fluxos alternativos)

	
2a. Não existem anúncios em que o tipo de regimento estipula que os critérios são objetivos.

>   O caso de uso termina.

2b. Todos os anúncios já estavam seriados antes do início do processo automático.

>   O caso de uso termina. 

2c. Não existem anúncios no período de seriação. 

>   O caso de uso termina. 

2d. A atribuição é opcional. 

> Continuação para o UC14.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
No arranque da aplicação.

#### Questões em aberto

* O que acontece se dois freelancers ficarem empatados na seriação?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Há algum motivo que possa levar à desclassificação de uma candidatura?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para a UC

![UC13_MD.svg](UC13_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * ProcessoSeriacao
 * TipoRegimento
 * Classificacao
 * Organizacao
 * Colaborador
 * Tarefa
 * AdjudicarAnuncio
 * GeradorNumeroSequencial
 * ProcessoAtribuicaoAdjudicacao
 * SeriarCandidaturaTask
 * Timer

Outras classes de software (i.e. Pure Fabrication) identificadas:

 * RegistoProcessoAtribuicaoAdjudicacao
 * RegistoAnuncios
 * RegistoOrganizacoes
 * RegistoAdjudicacao
 * ListaColaboradores
 * ListaCandidaturas
 * ListaTarefas
 * ListaClassificacao


| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. A aplicação é inicializada.  |	...é responsável por iniciar o UC à hora pretendida? | Timer |  |
| 2. O sistema identifica os anuncios cujo tipo de regimento estipula critérios objetivos, que estejam no periodo de seriação e que que ainda não tenham sido seriados e efetua a seriação e de seguida a sua atribuição, caso seja obrigatória. | ...faz a verificação dos anuncios? |  RegistoAnuncios |  O RegistoAnuncios possui todos os anuncios   |
| |	...seria os candidatos?  |   ProcessoSeriacao |  |
| | ...atribui o anuncio ao freelancer? | ProcessoAtribuicaoAdjudicacao || 
| |	...guarda a informação?  | SeriarCandidaturasAnunciosTask | |  	
|                        | ...valida os dados da ProcessoAtribuicaoAdjudicacao (validação global)? | RegistoAdjudicacao                | IE: O RegistoProcessoAtribuicaoAdjudicacao contém/agrega ProcessoAtribuicaoAdjudicacao |
|                        | ...valida os dados da ProcessoAtribuicaoAdjudicacao (validação global)? | Anuncio                           | IE: O Anuncio contém/agrega ProcessoAtribuicaoAdjudicacao    |
|                        | ...sabe a referência do anúncio?                             | Anúncio                           | O Anúncio contém a referência única da tarefa                |
|                        | ...sabe a descrição da tarefa?                               | Tarefa                            | O anúncio contém a referência da tarefa                      |
|                        | ...sabe a duração da tarefa?                                 | Tarefa                            | O anúncio contém a referência da tarefa                      |
|                        | ...cria o número sequencial (por ano)?                       | Interface GeradorNumeroSequencial | O GeradorNumeroSequencial gera um número sequencial único para a adjudicação |
|                        | ...sabe a data atual?                                        | Plataforma                        | A plataforma                         
|                        | ...sabe o valor aceite por ambas as partes?                  | Candidatura                       | A Candidatura sabe o valor concordante por ambas as partes   |
             

### Diagrama de Sequência

![UC13_SD.svg](UC13_SD.svg)

####GetListaAnunciosAutomaticosPorSeriar_SD

![getListaAnunciosAutomaticosPorSeriar_SD.svg](getListaAnunciosAutomaticosPorSeriar_SD.svg)

####SeriarCandidaturasSubmetidas_SD

![SeriarCandidaturasSubmetidas_SD.svg](SeriarCandidaturasSubmetidas_SD.svg)

####AtribuirCandidaturasSubmetidas_SD

![AtribuirCandidaturasSubmetidas_SD.svg](AtribuirCandidaturasSubmetidas_SD.svg)


### Diagrama de Classes

![UC13_CD.svg](UC13_CD.svg)
