# UC14 - Adjudicar/Atribuir Manualmente Anuncio

## 1. Engenharia de Requisitos

### Formato Breve

O gestor de organização inicia a adjudicação manual de um anúncio. O sistema apresenta a lista de anúncios seriados e pede que o Gestor de organização escolha um. O Gestor de organização escolhe um anúncio. O sistema mostra o melhor candidato à realização da tarefa associada ao anúncio. O Gestor confirma o candidato apresentado. O sistema apresenta os dados da adjudicação (i.e. organização, freelancer, descrição da tarefa, período da tarefa, valor aceite por ambas as partes, referência ao anúncio que lhe deu origem, número sequencial por ano, data da adjudicação) e pede confirmação. O gestor de organização confirma. O sistema regista a nova adjudicação, informando o gestor de organização do sucesso da operação.		

### SSD
![UC14_SSD.svg](UC14_SSD.svg)


### Formato Completo

#### Ator principal

Gestor de Organização

#### Partes interessadas e seus interesses
* **Gestor de Organização:** pretende fazer uma adjudicação de uma tarefa aquando do processo de atribuição.
* **T4J:** pretende que o gestor da organização registe uma adjudicação manual de um anúncio.


#### Pré-condições
n/a

#### Pós-condições
A informação do registo é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O Gestor de Organização inicia o registo de uma nova adjudicação manual de um anúncio.
2. O sistema apresenta a lista de anúncios seriados e pede que o Gestor de organização escolha um.
3. O Gestor de organização escolhe um anúncio.
4. O sistema mostra o melhor candidato à realização da tarefa associada ao anúncio.
5. O Gestor confirma o candidato apresentado.
6. O sistema apresenta os dados da adjudicação (i.e. organização, freelancer, descrição da tarefa, período da tarefa, valor aceite por ambas as partes, referência ao anúncio que lhe deu origem, número sequencial por ano, data da adjudicação) e pede confirmação.
7. O Gestor de Organização confirma. 
8. O sistema **regista a nova adjudicação no sistema, e informa o gestor de organização do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O gestor de organização solicita o cancelamento da adjudicação.

> O caso de uso termina.
	
2a. Não existem anúncios no sistema.
> 	1. O sistema informa o gestor de organização de tal. O caso de uso termina.

4a. Não existem candidatos no sistema.
> 	1. O sistema informa o gestor de organização de tal. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Caso haja mais que um candidato com a melhor candidatura, deve apresentar todos os candidatos ou apenas o que se candidatou mais cedo?
* Existem outros dados opcionais, ou são todos obrigatórios?
* Caso não haja anúncios, deve o caso de uso terminar?
* A adjudicação pode ter o mesmo número sequencial, em anos diferentes?
* É necessário o sistema validar todos os dados introduzidos?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para a UC

![UC14_MD.svg](UC14_MD.svg)

## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O gestor de organização inicia uma nova adjudicação de um anúncio|... interage com o utilizador?|ProcessoAtribuicaoAdjudicacaoUI|Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio.|
|... coordena o UC?| ProcessoAtribuicaoAdjudicacaoController | Controller (Modelo GRASP)|
|... cria instância de ProcessoAtribuicaoAdjudicacao | RegistoProcessoAtribuicaoAdjudicacao | Creator (Regra2)|
| 2. O sistema apresenta a lista de anúncios seriados e pede que o Gestor de organização escolha um.| ...conhece os anúncios existentes a listar?| RegistoAnuncios| IE: RegistoAnuncios tem/agrega todas os anúncios|
| 3. O Gestor de Organização seleciona um anúncio.|... guarda o anúncio selecionado?| ProcessoAtribuicaoAdjudicacao| IE: Armazena-se a referência do anúncio que é a referência da tarefa|
| 4. O sistema mostra o melhor candidato e pede confirmação.| ...conhece o candidato?| Candidatura| IE: Candidatura tem um freelancer associado|
| 5. O Gestor de Organização confirma o candidato.|... guarda o candidato selecionado?| ProcessoAtribuicaoAdjudicacao| IE: ProcessoAtribuicaoAdjudicacao| IE: ProcessoAtribuicaoAdjudicacao armazena o candidato - instância criada no passo 1|
| 6. O sistema apresenta os dados da adjudicação (i.e. organização, freelancer, descrição da tarefa, período da tarefa, valor aceite por ambas as partes, referência ao anúncio que lhe deu origem, número sequencial por ano, data da adjudicação) e pede confirmação.|...valida os dados do ProcessoAtribuicaoAdjudicacao (validação local)?| ProcessoAtribuicaoAdjudicacao  | IE: ProcessoAtribuicaoAdjudicacao  possui os seus próprios dados|
|| ...valida os dados da ProcessoAtribuicaoAdjudicacao (validação global)?| RegistoAdjudicacao | IE: O RegistoProcessoAtribuicaoAdjudicacao contém/agrega ProcessoAtribuicaoAdjudicacao | 
|| ...valida os dados da ProcessoAtribuicaoAdjudicacao (validação global)?| Anuncio | IE: O Anuncio contém/agrega ProcessoAtribuicaoAdjudicacao | 
||...sabe a referência do anúncio?| Anúncio | O Anúncio contém a referência única da tarefa|
||...sabe a descrição da tarefa?| Tarefa | O anúncio contém a referência da tarefa|
||...sabe a duração da tarefa?| Tarefa | O anúncio contém a referência da tarefa|
||...cria o número sequencial (por ano)?| Interface GeradorNumeroSequencial | O GeradorNumeroSequencial gera um número sequencial único para a adjudicação||
||...sabe a data atual?| Interface GeradorNumeroSequencial | A Interface GeradorNumeroSequencial | A Interface GeradorNumeroSequencial dá a hora em que a adjudicação ocorre |
||...sabe o valor aceite por ambas as partes?| Candidatura | A Candidatura sabe o valor concordante por ambas as partes |
| 7. O gestor de organização confirma.| ...guarda o ProcessoAtribuicaoAdjudicacao especificado/criado?| Anuncio | IE. O Anuncio contém/agrega ProcessoAtribuicaoAdjudicacao|
| 8. O sistema **regista a nova adjudicação no sistema, e informa o gestor de organização do sucesso da operação.| ... notifica o utilizador?| ProcessoAtribuicaoAdjudicacaoUI

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * Classificacao
 * Organizacao
 * Colaborador
 * ProcessoSeriacao
 * Tarefa
 * ProcessoAtribuicaoAdjudicacao
 * GeradorNumeroSequencial

Outras classes de software (i.e. Pure Fabrication) identificadas:

 * ProcessoAtribuicaoAdjudicacaoUI
 * ProcessoAtribuicaoAdjudicacaoController
 * RegistoAnuncios
 * RegistoOrganizacoes

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador
 * AplicacaoPOT

### Diagrama de Sequência

![UC14_SD.svg](UC14_SD.svg)

### Diagrama de Classes

![UC14_CD.svg](UC14_CD.svg)
