# Glossário

**Os termos devem estar organizados alfabeticamente.**

(Completar)

| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável por realizar na plataforma várias atividades de suporte ao negócio.|
| **ADM** | Acrónimo para Administrativo.|
| **Categoria (de Tarefa)** | Corresponde a uma descrição usada para catalogar uma ou mais tarefas (semelhantes).|
| **Colaborador (de Organização)** | Pessoa responsável por especificar tarefas. |
| **Organização** | Pessoa coletiva que pretende contratar _freelancers_ para a realização de tarefas necessárias à atividade da mesma.|
| **Gestor de Organização** | Pessoa individual responsável por realizar diversas tarefas alusivas à respetiva organização.|
| **_Freelancer_** | Pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo.|
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a plataforma informática.|
| **Tarefa** | Corresponde a pedidos por parte das organizações, com o fim de os _freelancers_ as executarem.|
| **Utilizador** | Pessoa que interage com a aplicação informática.|
| **Utilizador Não Registado** | Utilizador que interage com a plataforma informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto.|
| **Utilizador Registado** | Utilizador que interage com a plataforma informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Administrativo ou Gestor de Organização ou Colaborador de Organização ou _Freelancer_.|
| **Area de Atividade** | Corresponde a uma descrição usada para catalogar uma ou mais categorias (semelhantes). |
| **Competência Técnica** | Corresponde a uma descrição usada para catalogar competências requeridas para categorias de tarefas. |
| **Anuncio** | Corresponde a uma tarefa publicada pelo colaborador de organização para que freelancers se possam candidatar às mesmas.| 
| **Grau Proficiência** | Consiste num valor associado a uma competência técnica para que o sistema consiga validar a capacidade de um freelancer se candidatar ou não a um anuncio. |
| **Processo de seriação** | Corresponde à ordenação do candidatos a um anúncio. | 
| **Habilitação Académica** | Corresponde à preparação escolar do freelancer, inclui a instituição onde estudou e a sua média. |
| **Processo de atribuição/adjudicação** | Corresponde à atribuição de um anúncio a um freelancer. |
| **Classificacao** | Corresponde à classificação ordenada de candidaturas. |
| **Gerador sequencial de um número único por ano e data atual** | Gera um número único sequencial (por ano) e uma data da realização do processo de adjudicação. |
| **Tipo de Regimento** | Conjunto de critérios pelos quais se regem as seriações dos anúncios. |
| **Seriar Candidatura Task** | Tarefa que consiste na seriação de candidaturas referentes a anúncios agendada para ser executada periodicamente. |
| **Timer** | Responsável pela execução periódica da Seriar Candidatura Task. |
| **Candidatura** | Proposta efetuada por um freelancer a um anúncio. |








