﻿# UC6 - Especificar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a nova especificação de tarefa. O sistema os dados necessários (i.e. referência única por organização, designação, descrição formal, carácter técnico, duração, custo, categoria). O colaborador de organização introduz os dados solicitados. O sistema apresenta as categorias e solicita uma. O colaborador de organização seleciona uma. O sistema valida e apresenta os dados, pedindo que os confirme. O colaborador de organização confirma. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.

### SSD
![UC6_SSD.svg](UC6_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende especificar tarefas no sistema.
* **Organização:** pretende que os seus colaboradores possam especificar tarefas.
* **T4J:** pretende que existam tarefas no sistema.

#### Pré-condições
n/a

#### Pós-condições
A informação do registo é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a nova especificação de tarefa. 
2. O sistema solicita os dados necessários (i.e. referência única por organização, designação, descrição formal, carácter técnico, duração, custo).
3. O colaborador de organização introduz os dados solicitados.
4. O sistema apresenta as categorias.
5. O colaborador de organização seleciona uma categoria.
6. O sistema valida e apresenta os dados, pedindo que os confirme.
7. O colaborador de organização confirma.
8. O sistema **regista os dados** e informa o utilizador não registado do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da especificação de uma nova categoria.

> O caso de uso termina.

4a. Não existem categorias no sistema.
> 	1. O sistema informa o colaborador de organização de tal facto. O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3).
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3).
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

6c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o colaborador de organização para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 
	> 	2a. O colaborador de organização não altera os dados. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Se a plataforma não apresentar categorias, deve o colaborador de organização poder criar categorias?
* Caso não haja categorias, deve o caso de uso terminar?
* Todos os dados são obrigatórios?
* É necessário o sistema validar todos os dados introduzidos?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_MD.svg](UC6_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de Organização inicia a especificação de uma nova tarefa.| ... interage com o utilizador?| EspecificarTarefaUI| Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio.|
||...coordena o UC?|EspecificarTarefaController| Controller |
||...cria/instancia Tarefa?|ListaTarefas| Creator (Regra2)|
||...gera o identificador único da Tarefa?|ListaTarefas| IE: O ListaTarefas conhece todas as tarefas|
| 2. O sistema solicita os dados necessários (i.e. referência única por organização, designação, descrição formal, carácter técnico, duração, custo, categoria). | | | |
| 3. O colaborador de Organização introduz os dados solicitados. | ... guarda os dados introduzidos?| Tarefa | Information Expert (IE) - instância criada no passo 1|
| 4. O sistema mostra a lista de categorias para que seja selecionada uma.| ...conhece as categorias existentes a listar?| RegistoCategorias| IE: RegistoCategorias tem/agrega todas as categorias|
| 5. O colaborador de Organização seleciona a categoria em que se enquadra.| ... guarda a categoria selecionada?| Tarefa| IE: Tarefa enquadra-se numa categoria - instância criada anteriormente|
| 6. O sistema valida e apresenta os dados ao colaborador de Organização, pedindo que os confirme.| ...valida os dados da Tarefa (validação local)?| Tarefa| IE: Tarefa possui os seus próprios dados|
|| ...valida os dados da Tarefa (validação global)?| ListaTarefas | IE: A ListaTarefas contém/agrega Tarefa| 
| 7. O colaborador de Organização confirma.| ...guarda a Tarefa especificada/criada?| ListaTarefas | IE. A ListaTarefas contém/agrega Tarefa
| 8. O sistema regista os dados e informa o colaborador de Organização do sucesso da operação.| ... notifica o utilizador?| EspecificarTarefaUI                                                                                                                      


            
### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Categoria
 * Tarefa
 * Organização


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarTarefaUI  
 * EspecificarTarefaController
 * RegistoTarefa
 * RegistoCategorias


Outras classes de sistemas/componentes externos:

 * SessaoUtilizador

###	Diagrama de Sequência

![UC6_SD.svg](UC6_SD.svg)


###	Diagrama de Classes

![UC6_CD.svg](UC6_CD.svg)