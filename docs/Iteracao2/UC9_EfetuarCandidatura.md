﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿﻿# UC9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer efetua uma nova candidatura. O sistema apresenta a lista de anúncios disponíveis e pede ao freelancer que escolha um. O freelancer escolhe um anúncio. O sistema solicita dados(Valor pela tarefa, numero de dias para a concluir, texto de apresentação / motivação). O freelancer introduz os dados solicitados. O sistema apresenta os dados e solicita confirmação. O freelancer confirma. O sistema informa o freelancer do sucesso da afirmação e regista a candidatura.

### SSD
![UC9_SSD.svg](UC9_SSD.svg)


### Formato Completo

#### Ator principal
Freelancer

#### Partes interessadas e seus interesses
* **Freelancer** pretende efetuar candidatura.
* **T4J:** pretende que os freelancers possam candidatar-se a tarefas.
* **Organização:** pretende que os freelancers se candidatem às suas tarefas.

#### Pré-condições
Competências técnicas existentes possuirem grau de proficiência especificado.

#### Pós-condições
A informação da candidatura é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer efetua uma nova candidatura.
2. O sistema apresenta a lista de anúncios disponíveis e pede ao freelancer que escolha um.
3. O freelancer escolhe um anúncio.
4. O sistema solicita dados(Valor pela tarefa, numero de dias para a concluir, texto de apresentação / motivação).
5. O freelancer introduz os dados solicitados.
6. O sistema apresenta os dados e solicita confirmação.
7. O freelancer confirma.
8. O sistema informa o freelancer do sucesso da afirmação e regista a candidatura.

#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento da candidatura.

> O caso de uso termina.

2a. Não existem anúncios no sistema.
> 	1. O sistema informa o freelancer de tal. O caso de uso termina.

2b. O sistema deteta que o freelancer não possui o grau de proficiência mínimo.
>	1. O sistema alerta o freelancer para o facto.
>
	>	1a. O caso de uso termina.

5a. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o freelancer para o facto. 
> 	2. O sistema permite a sua alteração (passo 4).
> 
	> 	2a. O freelancer não altera os dados. O caso de uso termina.
    

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Se não houver nenhum freelancer elegivel para efetuar a tarefa?
* Existe um número máximo de dias para a execução?
* É necessário o sistema validar todos os dados introduzidos?
* Caso o freelancer não introduza algum dos dados obrigatórios, o caso de uso termina?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC9_MD.svg](UC9_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1.O freelancer efetua uma nova candidatura. | ... interage com o utilizador?| EfetuarCandidaturaUI| 	Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
||...coordena o UC?|EfetuarCandidaturaController| Controller |
||...cria a instancia de Candidatura? | ListaCandidatura | Creator (regra 1) O Anuncio tem candidatura. |
| 2.O sistema apresenta a lista de anúncios disponíveis/elegíveis e pede ao freelancer que escolha um. | ...contem os anúncios?| Plataforma| A plataforma contém/agrega Anuncio |
||...conhece todos os anuncios na plataforma? | RegistoAnuncio | HC + LC|
||...filtra os anuncios para o freelancer? | Plataforma | A plataforma possui o RregistoAnuncio |
| 3. O freelancer escolhe um anúncio.| ... guarda os dados introduzidos?| Candidatura | Information Expert (IE) - instância criada no passo 1|
| 4. O sistema solicita dados(Valor pela tarefa, numero de dias para a concluir, texto de apresentação/motivação).| ...verifica o grau de proficiência mínimo do freelancer| Anuncio | Anuncio tem acesso aos dados da competência técnica da tarefa e às habilitações do freelancer|
| 5. O freelancer introduz os dados solicitados.| ...guarda os dados introduzidos?| Candidatura | IE. A candidatura guarda os prórprios dados.|   
| 6. O sistema apresenta os dados e solicita confirmação. | | | |
| 7. O freelancer confirma. | ...valida os dados da Candidatura (validação local)?| Candidatura| IE: Candidatura possui os seus próprios dados| 
| | ...valida os dados da Candidatura (validação global)?| Anuncio | HC+LC. Anuncio conhece todas as candidaturas|
| 8. O sistema informa o freelancer do sucesso da afirmação e regista a candidatura. | ...informa o freelancer?| EfetuarCandidaturaUI| | 
||...guarda a Candidatura criada? | ListaCandidatura | HC + LC | A ListaCandidatura armazena todas as candidaturas referentes ao anuncio |


             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:
 
 * Plataforma
 * RegistoAnuncio
 * Categoria
 * Tarefa
 * Organização
 * Freelancer
 * Anuncio
 * RegistoAnuncio
 * Candidatura
 * ListaCandidatura

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EfetuarCandidaturaUI  
 * EfetuarCandidaturaController


###	Diagrama de Sequência

![UC9_SD.svg](UC9_SD.svg)

![getFreelancerbyEmail_SD](getFreelancerbyEmail_SD.svg)


###	Diagrama de Classes

![UC9_CD.svg](UC9_CD.svg)