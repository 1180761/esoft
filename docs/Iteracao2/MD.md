﻿﻿﻿﻿﻿﻿﻿﻿# Análise OO #
O processo de construção do modelo de domínio é baseado nos casos de uso, em especial os substantivos utilizados, e na descrição do enunciado.
## Racional para identificação de classes de domínio ##
Para a identificação de classes de domínio usa-se a lista de categorias das aulas TP (sugeridas no livro). Como resultado temos a seguinte tabela de conceitos (ou classes, mas não de software) por categoria.

### _Lista de Categorias_ ###

**Transações (do negócio)**

*

---

**Linhas de transações**

*

---

**Produtos ou serviços relacionados com transações**

*  Tarefa

---


**Registos (de transações)**

*  

---  


**Papéis das pessoas**

* Administrativo
* Freelancer
* Colaborador (de Organização)
* Gestor (de Organizacao)
* Utilizador
* Utilizador Não Registado

---


**Lugares**

*  Endereço Postal

---

**Eventos**

* 

---


**Objectos físicos**

*

---


**Especificações e descrições**

*  Área de Atividade
*  Competência Técnica
*  Tarefa
*  Categoria (de Tarefa)

---


**Catálogos**

*  

---


**Conjuntos**

*  

---


**Elementos de Conjuntos**

*  

---


**Organizações**

*  T4J (Plataforma)
*  Organização

---

**Outros sistemas (externos)**

*  (Componente Gestão Utilizadores)


---


**Registos (financeiros), de trabalho, contractos, documentos legais**

* 

---


**Instrumentos financeiros**

*  

---


**Documentos referidos/para executar as tarefas/**

* 
---



###**Racional sobre identificação de associações entre classes**###

Uma associação é uma relação entre instâncias de objectos que indica uma conexão relevante e que vale a pena recordar, ou é derivável da Lista de Associações Comuns:

+ A é fisicamente (ou logicamente) parte de B
+ A está fisicamente (ou logicamente) contido em B
+ A é uma descrição de B
+ A é conhecido/capturado/registado por B
+ A usa ou gere B
+ A está relacionado com uma transacção de B
+ etc.



| Conceito (A) 		|  Associação   		|  Conceito (B) |
|----------	   		|:-------------:		|------:       |
| Administrativo  		| define    	 	| Área de Atividade  |
|   					| define            | Competência Técnica  |
|   					| define            | Categoria |
|						| atua como			| Utilizador |
| 						| regista  			| Freelancer |
| Plataforma			| tem registadas    | Organização  |
|						| tem     			| Freelancer  |
|						| tem     			| Administrativo  |
| 						| possui   			| Competência Técnica  |
|						| possui     		| Área de Atividade  |
| 						| possui     		| Categoria |
| 						| possui     		| Tarefa  |
|  						| possui 			| Regimento |
| CompetenciaTecnica	| referente a       | Área de Atividade  |
| 						| aplica 			| GrauProficiencia |
| Categoria				| referente a       | Área de Atividade  |
| 						| referente a       | CompetênciaTécnica  |
| Organizacao			| localizada em | EndereçoPostal  |
|						| tem 	     	| Colaborador |
|						| tem    		| Gestor |
| Gestor			    | atua como	    | Colaborador |
|						| define		| Colaborador |
| Freelancer			| atua como		| Utilizador |
|						| possui 		| HabilitacaoAcademica | 
|						| efetua	    | Candidatura | 
| 						| possui  		| Reconhecimento |
|						| localizado em | Endereco |
| Candidatura 			| associada a 	| Anuncio | 
|						| efetuada por	| Freelancer | 
| Colaborador			| atua como		| Utilizador |
|   					| define        | Tarefa  |
| 						| define 		| SeriarAnuncio |
| 						| publica 		| Anuncio | 
| 						| participa 	| SeriarAnuncio | 
| Tarefa  				| referente a   | Categoria  |
| Anuncio  				| referente a 	| Tarefa | 
|						| possui 		| Regimento |
| CandidaturaSeriada 	| referente a 	| Candidatura | 
| SeriarAnuncio 		| possui 		| CandidaturaSeriada | 	
| Reconhecimento 		| referente a 	| GrauProficiencia |





## Modelo de Domínio

![MD.svg](MD.svg)

