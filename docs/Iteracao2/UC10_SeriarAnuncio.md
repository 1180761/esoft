﻿﻿# UC10 - Seriar Anuncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a seriação de um anuncio. O sistema apresenta a lista de anúncios publicados pelo colaborador e solicita um. O colaborador de organização seleciona um anuncio. O sistema apresenta a lista de candidaturas e solicita a classificação de cada uma. O colaborador de organização classifica as candidaturas. O sistema apresenta a lista de colaboradores da organização e solicita a seleção dos que participaram no processo. O colaborador de organização seleciona os colaboradores. O sistema solicita a data e hora em que o processo foi seriado. O colaborador de organização introduz os dados solicitados. O sistema apresenta os dados ao administrativo, pedindo que os confirme. O colaborador de organização confirma. O sistema informa o colaborador do sucesso da operação.

### SSD
![UC10_SSD.svg](UC10_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende seriar anúncios.
* **T4J:** pretende que os anuncios sejam seriados.

#### Pré-condições
n/a

#### Pós-condições
A informação do seriação é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a seriação de um anuncio.
2. O sistema apresenta a lista de anúncios publicados pelo colaborador e solicita um.
3. O colaborador de organização seleciona um anuncio.
4.  O sistema apresenta a lista de candidaturas e solicita a classificação de cada uma.
5. O colaborador de organização classifica as candidaturas.
6. O sistema apresenta a lista de colaboradores da organização e solicita a seleção dos que participaram no processo.
7. O colaborador de organização seleciona os colaboradores. 
8. O sistema solicita a data e hora em que o processo foi seriado.
9. O colaborador de organização introduz os dados solicitados. 
10. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
11. O colaborador de organização confirma. 
12. O sistema informa o colaborador do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da especificação de uma nova categoria.

> O caso de uso termina.

2a. Não existem anuncios no sistema.

> 	1. O sistema informa o colaborador de organização de tal facto. O caso de uso termina.

4a. Não existem candidaturas no sistema.

> 		1. O sistema informa o colaborador de organização de tal facto. O caso de uso termina.

6a. Não existem colaboradores da organização no sistema.

> 		1. O sistema informa o colaborador de organização de tal facto.

10a. Dados mínimos obrigatórios em falta.

>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3).
>	
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

10b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.

>	1. O sistema alerta o colaborador de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3).
>	
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

10c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

> 	1. O sistema alerta o colaborador de organização para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 	
	> 	2a. O colaborador de organização não altera os dados. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Caso não haja candidaturas, deve o caso de uso terminar?
* Todos os dados são obrigatórios?
* É necessário o sistema validar todos os dados introduzidos?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD.svg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a seriação de um anuncio. | ... interage com o utilizador?| SeriarAnuncioUI | 	Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
||...coordena o UC?|SeriarAnuncioController| Controller |
||...cria/instancia SeriarAnuncio?|RegistoSeriarAnuncios| Creator (regra 1) + HC +LC sobre Plataforma |
| 2. O sistema apresenta a lista de anúncios publicados pelo colaborador e solicita um. | ...conhece os anuncios a listar? | RegistoAnuncios | IE: RegistoAnuncios tem/agrega todas os anuncios |
| 3. O colaborador de organização seleciona um anuncio. | ... guarda o anuncio? | SeriarAnuncio | Information Expert (IE) - instância criada no passo 1,no MD cada SeriarAnuncio tem um anuncio |
| 4. O sistema apresenta a lista de candidaturas e solicita a classificação de cada uma. | ...conhece as candidaturas existentes a listar? | Anuncio | IE: Anuncio tem/agrega candidaturas |
| 5. O colaborador de organização classifica uma candidatura. | ... guarda a classificacao? | SeriarAnuncio | IE: SeriarAnuncio tem CandidaturaClassificada |
| 6. Os passos 4 a 5 repetem-se enquanto não forem classificadas todas as candidaturas. |                                                  |                         |                                                              |
| 7. O sistema apresenta a lista de colaboradores e solicita a escolha de um participante. | ...conhece os colaboradores existentes a listar? | Organizacao             | IE: Organizacao tem/agrega colaboradores                     |
| 8. O colaborador de organização seleciona um participante.   | guarda o colaborador?                            | SeriarAnuncio           | IE: SeriarAnuncio tem/agrega colaboradores                   |
| 9. Os passos 7 a 8 repetem-se enquanto não forem introduzidas todos os participantes. |                                                  |                         |                                                              |
| 10. O sistema solicita a data e hora em que o processo foi seriado. |                                                  |                         |  |
| 11. O colaborador de organização introduz os dados solicitados. | guarda os dados introduzidos? | SeriarAnuncio | |
| 12. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. | ...valida os dados do SeriarAnuncio (validação local)            | SeriarAnuncio | IE. A SeriarAnuncio possui os seus próprios dados.               |
|                                                              | ...valida os dados da SeriarAnuncio (validação global)           | RegistoSeriarAnuncio           | IE: O RegistoSeriarAnuncio possui/agrega SeriarAnuncio .             |
| 13. O colaborador de organização confirma.                               |                                                              |                             |                                                              |
| 14. O sistema regista os dados e informa o colaborador de organização do sucesso da operação. | ... guarda o SeriarAnuncio criado?                     | RegistoSeriarAnuncio            | IE: O RegistoSeriarAnuncio  possui/agrega SeriarAnuncio.             |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Colaborador
 * Anuncio
 * Organização
 * SeriarAnuncio
 * CandidaturaClassificada


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController
 * RegistoOrganizacoes
 * RegistoAnuncios
 * RegistoSeriarAnuncios


###	Diagrama de Sequência

![UC10_SD.svg](UC10_SD.svg)


###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)