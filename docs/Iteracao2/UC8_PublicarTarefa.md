﻿﻿# UC8 - Publicar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a publicação de uma tarefa. O sistema solicita dados (período de publicitação, período de apresentação, período de seriação, periodo de decisão e uma tarefa). O colaborador de organização introduz os dados solicitados. O sistema mostra a lista de tarefas e pede para selecionar uma. O colaborador de organização seleciona uma tarefa. O sistema mostra a lista de tipos de regime e pede para selecionar um. O colaborador de organização seleciona um tipo de regime. O sistema apresenta os dados e solicita confirmação. O colaborador de organização confirma. O sistema regista o novo colaborador da organização, informando o colaborador de organização do sucesso da operação.

SSD

### SSD
![UC8_SSD.svg](UC8_SSD.svg)


### Formato Completo

#### Ator principal
Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende publicar anúncios de tarefas no sistema.
* **Plataforma:** pretende que os seus colaboradores possam publicar tarefas.
* **Freelancer:** pretende conhecer as tarefas a que pode candidatar-se.
* **T4J:** pretende a adjudicação de tarefas a freelancers.

#### Pré-condições
Existir pelo menos um tipo de regimento no sistema.

#### Pós-condições
A informação do registo é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a publicação de uma tarefa.
2. O sistema solicita dados (período de publicitação, período de apresentação, período de seriação, periodo de decisão).
3. O colaborador de organização introduz os dados solicitados.
4. O sistema mostra a lista de tarefas e pede para selecionar uma.
5. O colaborador de organização seleciona uma tarefa.
6. **O sistema mostra a lista de tipo de regimes existentes e pede ao colaborador para selecionar um.**
7. O colaborador de organização seleciona um tipo de regime.
8. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.
9. O colaborador de organização confirma.
10. O sistema **regista os dados** e informa o colaborador de organização do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da especificação de uma publicação de uma tarefa.

> O caso de uso termina.

4a. Não existem tarefas no sistema.
> 	1. O sistema informa o colaborador de organização de tal facto.
>	2. O sistema permite a definição de uma nova tarefa (UC6 - Especificar Tarefa).
>	
	>	2a. O colaborador de organização não define uma nova tarefa. O caso de uso termina.

5a. O colaborador de organização informa que pretende usar uma outra tarefa.
>   1. O sistema permite definição de uma nova tarefa (UC6 - Especificar Tarefa).
>   1. O sistema volta para o passo 4.

8a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3).
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

8b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 	1. O sistema alerta o colaborador de organização para o facto. 
> 	2. O sistema permite a sua alteração (passo 3).
> 
	> 	2a. O colaborador de organização não altera os dados. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto
 
* Caso não haja tarefas, deve o caso de uso terminar?
* Todos os dados são obrigatórios?
* É necessário o sistema validar todos os dados introduzidos?
* Qual a frequência de ocorrência deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC8_MD.svg](UC8_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a publicação de uma tarefa.| ... interage com o utilizador?| AnuncioUI| Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio.|
||...coordena o UC?|AnuncioController| Controller |
||...cria/instancia Anuncio?|ListaAnuncios| Creator (Regra2)|
||...gera o identificador único do Anúncio?|RegistoAnuncios| I.E: O RegistoAnuncios conhece todas os anúncios|
| 2. O sistema solicita os dados necessários (i.e. período de publicitação, período de apresentação, período de seriação, periodo de decisão). | | | |
| 3. O colaborador de Organização introduz os dados solicitados. | ... guarda os dados introduzidos?| Anúncio | Information Expert (IE) - instância criada no passo 1|
| 4. O sistema mostra a lista de tarefas e pede para selecionar uma.| ...conhece as tarefas existentes a listar?| ListaTarefas| IE: ListaTarefas tem/agrega todas as tarefas|
||...sabe a que tarefa se refere o anúncio?| Anúncio | O Anúncio recebe a referência única da tarefa selecionada|
| 5. O colaborador de organização seleciona uma tarefa.| ... guarda a tarefa selecionada?| Anúncio| IE: Anúncio enquadra-se numa Tarefa - instância criada anteriormente|
| 6. O sistema mostra a lista de tipo de regimes existentes e pede ao colaborador para selecionar um.| .. conhece os tipos de regime?| RegistoRegimes| IE: RegistoRegimes possui tipos de regime|
| 7. O colaborador de organização seleciona um tipo de regime.| ... guarda o tipo de regime selecionado?| Anuncio| IE: O tipo de regime enquadra-se no respetivo Anúncio - instância criada anteriormente|
| 8. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.| ...valida os dados do Anúncio (validação local)?| Anúncio | IE: Anúncio possui os seus próprios dados|
|| ...valida os dados da Tarefa (validação global)?| RegistoAnúncio | IE: O RegistoAnúncio contém/agrega Anúncio| 
| 9. O colaborador de organização confirma.| ...guarda o anúncio especificado/criado?| RegistoAnuncios | IE. O RegistoAnuncios contém/agrega Anúncio
| 10. O sistema regista os dados e informa o colaborador de Organização do sucesso da operação.| ... notifica o utilizador?| AnuncioUI   
                                                   


             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anúncio
 * Organização


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AnúncioUI  
 * AnúncioController
 * RegistoAnuncios
 * RegistoRegimes
 * ListaTarefas

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador


###	Diagrama de Sequência

![UC8_SD.svg](UC8_SD.svg)


###	Diagrama de Classes

![UC8_CD.svg](UC8_CD.svg)