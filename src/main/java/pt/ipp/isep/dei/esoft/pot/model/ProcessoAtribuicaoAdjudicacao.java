package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

import java.util.Set;

public class ProcessoAtribuicaoAdjudicacao {

    private TipoRegimento tipoRegime;
    private Date data;
    private boolean obrigatorio;
    private Freelancer freelancer;
    private Organizacao m_oOrg;
    double valorAceite;
    String descTarefa;
    double periodoTarefa;
    String refAnuncio;
    Date dAdju;
    int nSeqAno;

    public ProcessoAtribuicaoAdjudicacao(Organizacao org, Freelancer freelancer, String descTarefa, double periodoTarefa, double valorAceite, String refAnuncio, int nSeqAno, Date dAdju){
        this.m_oOrg = org;
        this.freelancer = freelancer;
        this.descTarefa = descTarefa;
        this.periodoTarefa = periodoTarefa;
        this.valorAceite = valorAceite;
        this.refAnuncio = refAnuncio;
        this.nSeqAno = nSeqAno;
        this.dAdju = dAdju;
    }

    public TipoRegimento getTipoRegime() {
        return tipoRegime;
    }
    public void setTipoRegime(TipoRegimento tipoRegime) {
        this.tipoRegime = tipoRegime;
    }

    public boolean isObrigatorio() {
        return obrigatorio;
    }

    public void setObrigatorio(boolean obrigatorio) {
        this.obrigatorio = obrigatorio;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean valida(){
        return true;
    }

    public Freelancer getFreelancer() {
        return freelancer;
    }

    public void setFreelancer(Freelancer freelancer) {
        this.freelancer = freelancer;
    }

    public Organizacao getM_oOrg() {
        return m_oOrg;
    }

    public void setM_oOrg(Organizacao m_oOrg) {
        this.m_oOrg = m_oOrg;
    }

    @Override
    public String toString() {
        return "ProcessoAtribuicaoAdjudicacao{" +
                "tipoRegime=" + tipoRegime +
                ", data=" + data +
                ", obrigatorio=" + obrigatorio +
                ", freelancer=" + freelancer +
                ", m_oOrg=" + m_oOrg +
                ", valorAceite=" + valorAceite +
                ", descTarefa='" + descTarefa + '\'' +
                ", periodoTarefa=" + periodoTarefa +
                ", refAnuncio='" + refAnuncio + '\'' +
                ", dAdju=" + dAdju +
                ", nSeqAno=" + nSeqAno +
                '}';
    }
}
