package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Set;

public class RegistoFreelancers {

    private final Set<Freelancer> m_listaFreelancers;

    public RegistoFreelancers(){
        this.m_listaFreelancers = new HashSet<>();
    }

    public Set<Freelancer> getM_listaFreelancers() {
        return m_listaFreelancers;
    }

    public boolean registaFreelancer(Freelancer freelancer){
        if (validaFreelancer()) addFreelancer(freelancer);
        return false;
    }

    public Freelancer getFreelancerbyEmail(String strEmail){
        for(Freelancer f : m_listaFreelancers){
            if(f.getStrEmail().equals(strEmail)){
                return f;
            }
        }
        return null;
    }

    private boolean addFreelancer(Freelancer freelancer){
        return m_listaFreelancers.add(freelancer);
    }

    private boolean validaFreelancer(){
        return true;
    }
}
