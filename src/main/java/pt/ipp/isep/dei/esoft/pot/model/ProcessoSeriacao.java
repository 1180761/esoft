package pt.ipp.isep.dei.esoft.pot.model;

import java.util.*;

public class ProcessoSeriacao {

    private TipoRegimento tipoRegime;
    private Set<Classificacao> classificacoes;
    private Set<Colaborador> participantes;
    private Date data;
    private String conclusao;


    public ProcessoSeriacao(TipoRegimento tipoRegime, Colaborador colaborador) {
        this.tipoRegime = tipoRegime;
        this.participantes = new HashSet<>();
        this.classificacoes = new HashSet<>();
        this.participantes.add(colaborador);
    }

    public ProcessoSeriacao(TipoRegimento tipoRegime){
        this.tipoRegime = tipoRegime;
    }


    public Set<Colaborador> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(Set<Colaborador> participantes) {
        this.participantes = participantes;
    }

    public TipoRegimento getTipoRegime() {
        return tipoRegime;
    }

    public void setTipoRegime(TipoRegimento tipoRegime) {
        this.tipoRegime = tipoRegime;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getConclusao() {
        return conclusao;
    }

    public void setConclusao(String conclusao) {
        this.conclusao = conclusao;
    }

    public boolean addColaborador(Colaborador colaborador){
        return participantes.add(colaborador);
    }

    public void sortListaCandidaturas(){
        List<Classificacao> list = (List) classificacoes;
        list.sort(new Comparator<Classificacao>() {
            @Override
            public int compare(Classificacao t, Classificacao t1) {
                int i;
                i = Double.compare(t.getClassificacao(), t1.getClassificacao());
                return i;
            }
        });
        setClassificacoes((Set) classificacoes);
    }


    public boolean addCandidaturaClassificada(Candidatura candidatura, int classificacao, String justificacao) {
        Classificacao classificacao1 = new Classificacao(candidatura, classificacao, justificacao);
        if (validaCandidaturaClassificadas(classificacao1)) {
            return classificacoes.add(classificacao1);
        }
        return false;
    }

    public boolean addCandidaturaClassificada(Candidatura candidatura, int classificacao) {
        Classificacao classificacao1 = new Classificacao(candidatura, classificacao);
        if (validaCandidaturaClassificadas(classificacao1)) {
            return classificacoes.add(classificacao1);
        }
        return false;
    }

    public boolean validaCandidaturaClassificadas (Classificacao classificacao){
        return classificacao != null;
    }

    public Set<Classificacao> getClassificacoes() {
        return classificacoes;
    }

    public void setClassificacoes(Set<Classificacao> classificacoes) {
        this.classificacoes = classificacoes;
    }

    public boolean valida(){
        return true;
    }

    public int getOrdemByCandidatura(Candidatura candidatura){
        return 0;
    }

    @Override
    public String toString() {
        return "ProcessoSeriacao{" +
                "tipoRegime=" + tipoRegime +
                ", candidaturaClassificadas=" + classificacoes +
                ", participantes=" + participantes +
                ", data='" + data + '\'' +
                '}';
    }
}
