package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RegistoRegimes {

    private final Set<TipoRegimento> m_lstRegimes;

    public RegistoRegimes() {
        m_lstRegimes = new HashSet<>();
    }

    public TipoRegimento novoRegimento(String designacao, String descricaoRegras, boolean objetivo) {
        int id = m_lstRegimes.size() + 1;
        return new TipoRegimento(designacao,descricaoRegras,objetivo);
    }

    private boolean addRegimento(TipoRegimento regimento) {
        return m_lstRegimes.add(regimento);
    }

    public Set<TipoRegimento> getListaRegimes() {
        return m_lstRegimes;
    }

}