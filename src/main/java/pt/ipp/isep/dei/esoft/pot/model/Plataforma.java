﻿package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import java.util.Random;
import java.util.Set;

import java.util.Timer;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Plataforma implements GeradorNumeroSequencial {

    private String m_strDesignacao;
    private final AutorizacaoFacade m_oAutorizacao;
    private final RegistoOrganizacoes m_oRegistoOrganizacoes;
    private final RegistoAreasAtividade m_oRegistoAreasAtividade;
    private final RegistoCompetenciasTecnicas m_oRegistoCompetenciasTecnicas;
    private final RegistoCategorias m_oRegistoCategorias;
    private final RegistoAnuncios m_oRegistoAnuncios;
    private final RegistoFreelancers m_oFreelancers;
    private final RegistoRegimes m_oRegistoRegimes;

    private long delay;
    private long intervalo;
    private SeriarCandidaturaTask task;
    private Timer timer;

    public Plataforma(String strDesignacao) {
        if ((strDesignacao == null)
                || (strDesignacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;

        this.m_oAutorizacao = new AutorizacaoFacade();
        this.m_oRegistoOrganizacoes = new RegistoOrganizacoes(m_oAutorizacao);
        this.m_oRegistoAreasAtividade = new RegistoAreasAtividade();
        this.m_oRegistoCompetenciasTecnicas = new RegistoCompetenciasTecnicas();
        this.m_oRegistoCategorias = new RegistoCategorias();
        this.m_oRegistoAnuncios = new RegistoAnuncios();

        this.m_oFreelancers = new RegistoFreelancers();

        this.m_oRegistoRegimes = new RegistoRegimes();

        agendaSeriacao();   //Seriar no arranque da aplicação

    }

    public String getDesignacao() {
        return m_strDesignacao;
    }

    public AutorizacaoFacade getAutorizacao() {
        return m_oAutorizacao;
    }

    public RegistoOrganizacoes getRegistoOrganizacoes() {
        return m_oRegistoOrganizacoes;
    }

    public RegistoAreasAtividade getRegistoAreasAtividade() {
        return m_oRegistoAreasAtividade;
    }

    public RegistoCompetenciasTecnicas getRegistoCompetenciasTecnicas() {
        return m_oRegistoCompetenciasTecnicas;
    }

    public RegistoCategorias getRegistoCategorias() {
        return m_oRegistoCategorias;
    }

    public RegistoFreelancers getRegistoFreelancers() {
        return m_oFreelancers;
    }

    public RegistoAnuncios getRegistoAnuncios() {
        return m_oRegistoAnuncios;
    }

    public RegistoRegimes getRegistoRegimes(){return m_oRegistoRegimes;}

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public long getIntervalo() {
        return intervalo;
    }

    public void setIntervalo(long intervalo) {
        this.intervalo = intervalo;
    }

    private String gerarPassword(){return "";}

    private void enviarEmail(String email, String pswd){
        //do enviatings do email
    }

    private void agendaSeriacao(){
       Set<Anuncio> listaAnunciosAutomaticosPorSeriar = m_oRegistoAnuncios.getM_lstAnunciosAutomaticosPorSeriar();
       task = new SeriarCandidaturaTask(listaAnunciosAutomaticosPorSeriar);
       timer.schedule(task,delay,intervalo);
    }

    @Override
    public int geraNumSequencial() {
        Random rand = new Random();
        int random = rand.nextInt(100);
        Date date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        String s1 = Integer.toString(random);
        String s2 = Integer.toString(date.getYear());
        String s = s1 + s2;

        return Integer.parseInt(s);
    }

    @Override
    public Date getDataAtual() {
        return Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
