package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Set;

public class ListaCandidatura {
    private final Set<Candidatura> m_listaCandidaturas;

    public ListaCandidatura(){
        this.m_listaCandidaturas = new HashSet<>();
    }

    public Candidatura novaCandidatura(Freelancer freelancer, double valorTarefa, int numDias, String txtApresentacao, String txtMotivacao, Anuncio anuncio){
        return new Candidatura(freelancer,valorTarefa,numDias,txtApresentacao,txtMotivacao, anuncio);
    }

    public Candidatura novaCandidatura(Freelancer freelancer, double valorTareda, int numDias, Anuncio anuncio){
        return new Candidatura(freelancer, valorTareda,numDias, anuncio);
    }

    public boolean registaCandidatura(Candidatura candidatura){
        if(validaCandidatura(candidatura)){
            m_listaCandidaturas.add(candidatura);
        }
        return false;
    }

    private boolean validaCandidatura(Candidatura candidatura){
        return candidatura != null;
    }

    public Set<Candidatura> getM_listaCandidaturas() {
        return m_listaCandidaturas;
    }

    public void addCandidatura(Candidatura candidatura){
        m_listaCandidaturas.add(candidatura);
    }

}
