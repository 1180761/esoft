package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.PublicarTarefaController;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Regimento;
import pt.ipp.isep.dei.esoft.pot.model.RegistoRegimes;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class PublicarTarefaUI {

    private PublicarTarefaController m_controller;
    public PublicarTarefaUI()
    {
        m_controller = new PublicarTarefaController();
    }

    public void run()
    {
        System.out.println("\nPublicar Tarefa:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.publicaAnuncio()) {
                    System.out.println("Publicação efetuada com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir a publicação com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        Date pPublicitacao = (Utils.readDateFromConsole("Periodo de Publicitação: "));
        Date pApresentacao = (Utils.readDateFromConsole("Periodo de Apresentação: "));
        Date pSeriacao = Utils.readDateFromConsole("Periodo de Seriação: ");
        Date pDecisao = Utils.readDateFromConsole("Periodo de Decisão: ");

        List<Tarefa> tarefas = m_controller.getLstTarefasByMailColab();

        Tarefa tarefa = null;
        ListaTarefas tar = (ListaTarefas) Utils.apresentaESeleciona(tarefas, "Selecione a Tarefa a que é referente esta publicação:");
        if (tar != null)
            tarefa = tar.getTarefaByRefUnica(tarefa);

        return m_controller.novoAnuncio(pPublicitacao, pApresentacao, pSeriacao, pDecisao, tarefa);
    }

    private void apresentaDados()
    {
        System.out.println("\nAnúncio:\n" + m_controller.getAnuncioString());
    }
}
