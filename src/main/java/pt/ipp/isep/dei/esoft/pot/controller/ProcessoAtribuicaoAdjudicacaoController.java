package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcessoAtribuicaoAdjudicacaoController {

    private Plataforma m_oPlataforma;
    private RegistoOrganizacoes m_oRegistoOrganizacoes;
    private Organizacao m_oOrganizacao;
    private RegistoAnuncios m_oRegistoAnuncios;
    private Anuncio m_oAnuncio;
    private SessaoUtilizador m_oSessaoUtilizador;
    private Candidatura m_oCandidatura;
    private ProcessoSeriacao m_oProcessoSeriacao;
    private String email;
    private List<Anuncio> m_lstAnunciosSeriados;
    private Colaborador colaborador;
    private ProcessoAtribuicaoAdjudicacao processoAtribuicaoAdjudicacao;


    public ProcessoAtribuicaoAdjudicacaoController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_GESTOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
        this.m_oSessaoUtilizador = AplicacaoPOT.getInstance().getSessaoAtual();
        this.email = this.m_oSessaoUtilizador.getEmailUtilizador();
        this.m_oRegistoOrganizacoes = this.m_oPlataforma.getRegistoOrganizacoes();
        this.m_oOrganizacao = this.m_oRegistoOrganizacoes.getOrganizacaoByEmailColab(this.email);
        this.m_oRegistoAnuncios = m_oPlataforma.getRegistoAnuncios();
        this.colaborador = m_oOrganizacao.getColaboradorByEmail(email);
        this.m_lstAnunciosSeriados = m_oRegistoAnuncios.getAnunciosSeriados();
    }

    public ProcessoAtribuicaoAdjudicacao novaAdjudicacao(){
        Tarefa tarefa = m_oAnuncio.getTarefa();
        String descTarefa = tarefa.getDesignacao();
        String refAnuncio = tarefa.getRefUnica();
        double pTarefa = tarefa.getDuracao();
        double vAceite = m_oCandidatura.getValorTarefa();
        int nSeqAno = m_oPlataforma.geraNumSequencial();
        Date dAdjud = m_oPlataforma.getDataAtual();
        processoAtribuicaoAdjudicacao = m_oAnuncio.novoProcessoAtribuicao(m_oOrganizacao,m_oCandidatura.getM_oFreelancer(),descTarefa,pTarefa,vAceite,refAnuncio,nSeqAno, dAdjud);
        return processoAtribuicaoAdjudicacao;
    }

    public boolean AdjudicaAnuncio(){
        return m_oAnuncio.registaProcessoAtribuicao(processoAtribuicaoAdjudicacao);
    }

    public Freelancer getMelhorCandidato(Anuncio m_oAnuncio){
        this.m_oAnuncio = m_oAnuncio;
        m_oProcessoSeriacao = m_oAnuncio.novoProcessoSeriacao(colaborador);
        m_oCandidatura = m_oProcessoSeriacao.getClassificacoes().iterator().next().getCandidatura();
        return m_oCandidatura.getM_oFreelancer();
    }

    public Anuncio getAnuncioByIdTarefa(String anuncioTarefaId){
        m_oAnuncio = m_oRegistoAnuncios.getAnuncioByIdTarefa(anuncioTarefaId);
        return m_oAnuncio;
    }
    public List<Anuncio> getAnunciosSeriados(){
        return m_oRegistoAnuncios.getAnunciosSeriados();
    }

    public String getProcessoAtribuicaoAdjudicacaoString()
    {
        return this.processoAtribuicaoAdjudicacao.toString();
    }
}
