package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;
import java.util.Objects;
/**
 *
 * @author Fausto
 */

public class Tarefa {

    private String refUnica;
    private String designacao;
    private String descInformal;
    private String descCaracterTecnico;
    private double duracao;
    private double custo;
    private Categoria categoria;
    private String mailColab;
    private Freelancer m_oFreelancer;

    public Tarefa(String refUnica, String designacao, String descInformal, String descCaracterTecnico, double duracao, double custo, Categoria categoria, String mailColab) {
        if ((refUnica == null) || (designacao == null) || (descInformal == null) ||  (descCaracterTecnico == null) || (duracao == 0) || (custo == 0) || (categoria == null) || (mailColab == null))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.refUnica = refUnica;
        this.designacao = designacao;
        this.descInformal = descInformal;
        this.descCaracterTecnico = descCaracterTecnico;
        this.duracao = duracao;
        this.custo = custo;
        this.categoria = categoria;
        this.mailColab = mailColab;
    }

    public String getRefUnica(){
        return refUnica;
    }
    public void setRefUnica(String a){
        refUnica = a;
    }
    public String getDesignacao(){
        return designacao;
    }
    public void setDesignacao(String a){
        designacao = a;
    }
    public String getDescInformal(){
        return descInformal;
    }
    public void setDescInformal(String a){
        descInformal = a;
    }
    public String getDescCaracterTecnico(){
        return descCaracterTecnico;
    }
    public void setDescCaracterTecnico(String a){
        descCaracterTecnico = a;
    }
    public double getDuracao(){
        return duracao;
    }
    public void setDuracao(double a){
        duracao = a;
    }
    public double getCusto(){
        return custo;
    }
    public void setCusto(double a){
        custo = a;
    }
    public Categoria getCategoria() {
        return categoria;
    }
    public void setCategoria(Categoria selectCategoria) {
        this.categoria = selectCategoria;
    }
    public String getMailColab() {
        return mailColab;
    }
    public void setMailColab(String mailColab) {
        this.mailColab = mailColab;
    }

    @Override
    public String toString() {
        return "Tarefa referente a {" +
                "Referência Única = '" + refUnica + '\'' +
                ", Designação = '" + designacao + '\'' +
                ", Descrição informal = " + descInformal +
                ", Descrição de carácter técnico = " + descCaracterTecnico +
                ", duração = " + duracao +
                ", custo = " + custo +
                '}';
    }


    public Freelancer getM_oFreelancer() {
        return m_oFreelancer;
    }

    public void setM_oFreelancer(Freelancer m_oFreelancer) {
        this.m_oFreelancer = m_oFreelancer;
    }
}
