package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class CaraterCT {

    private boolean obrigatorio;
    private GrauProficiencia grauProficiencia;
    private CompetenciaTecnica competenciaTecnica;

    public CaraterCT(boolean obrigatorio, GrauProficiencia grauProficiencia, CompetenciaTecnica competenciaTecnica){
        this.obrigatorio = obrigatorio;
        this.grauProficiencia = grauProficiencia;
        this.competenciaTecnica = competenciaTecnica;
    }

    public boolean isObrigatorio() {
        return obrigatorio;
    }

    public void setObrigatorio(boolean obrigatorio) {
        this.obrigatorio = obrigatorio;
    }

    public GrauProficiencia getGrauProficiencia() {
        return grauProficiencia;
    }

    public void setGrauProficiencia(GrauProficiencia grauProficiencia) {
        this.grauProficiencia = grauProficiencia;
    }

    public CompetenciaTecnica getCompetenciaTecnica() {
        return competenciaTecnica;
    }

    public void setCompetenciaTecnica(CompetenciaTecnica competenciaTecnica) {
        this.competenciaTecnica = competenciaTecnica;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaraterCT caraterCT = (CaraterCT) o;
        return obrigatorio == caraterCT.obrigatorio &&
                Objects.equals(grauProficiencia, caraterCT.grauProficiencia) &&
                Objects.equals(competenciaTecnica, caraterCT.competenciaTecnica);
    }

    @Override
    public int hashCode() {
        return Objects.hash(obrigatorio, grauProficiencia, competenciaTecnica);
    }

    @Override
    public String toString() {
        return "CaraterCT{" +
                "obrigatorio=" + obrigatorio +
                ", grauProficiencia=" + grauProficiencia +
                ", competenciaTecnica=" + competenciaTecnica +
                '}';
    }
}
