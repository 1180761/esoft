package pt.ipp.isep.dei.esoft.pot.model;

public class Regimento {
    private String descricao;

    public Regimento(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
