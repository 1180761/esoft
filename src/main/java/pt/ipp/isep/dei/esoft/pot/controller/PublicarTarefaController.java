package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PublicarTarefaController {

    private Plataforma m_oPlataforma;
    private RegistoOrganizacoes m_oRegistoOrganizacoes;
    private Organizacao m_oOrganizacao;
    private RegistoAnuncios m_oRegistoAnuncios;
    private Anuncio m_oAnuncio;
    private Tarefa m_oTarefa;
    private ListaTarefas m_oListaTarefas;
    private SessaoUtilizador m_oSessaoUtilizador;
    private RegistoRegimes m_oRegistoRegimes;
    private String email;
    private List<Tarefa> m_lstTarefasByMailColab;


    public PublicarTarefaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oListaTarefas = m_oListaTarefas;
        this.m_lstTarefasByMailColab = m_lstTarefasByMailColab;
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
        this.m_oSessaoUtilizador = AplicacaoPOT.getInstance().getSessaoAtual();
        this.email = this.m_oSessaoUtilizador.getEmailUtilizador();
        this.m_oRegistoOrganizacoes = this.m_oPlataforma.getRegistoOrganizacoes();
        this.m_oOrganizacao = this.m_oRegistoOrganizacoes.getOrganizacaoByEmailColab(this.email);
        this.m_oRegistoRegimes = this.m_oPlataforma.getRegistoRegimes();
    }

    public boolean novoAnuncio(Date pPublicitacao, Date pApresentacao, Date pSeriacao, Date pDecisao, Tarefa tarefa){
        try{
            this.m_lstTarefasByMailColab = this.m_oListaTarefas.getListaTarefasByMailColab(email);
            this.m_oRegistoAnuncios = this.m_oPlataforma.getRegistoAnuncios();
            this.m_oRegistoOrganizacoes = this.m_oPlataforma.getRegistoOrganizacoes();
            this.m_oRegistoRegimes = this.m_oPlataforma.getRegistoRegimes();
            this.m_oTarefa = this.m_oListaTarefas.getTarefaByRefUnica(tarefa);
            this.m_oAnuncio = m_oRegistoAnuncios.novoAnuncio(pPublicitacao, pApresentacao, pSeriacao, pDecisao, tarefa, email);
            return m_oRegistoAnuncios.validarNovoAnuncio(m_oAnuncio);
        }catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oAnuncio = null;
            return false;
        }
    }

    public void setRegime(TipoRegimento tipoRegime) {
        m_oAnuncio.setTipoRegime(tipoRegime);
    }

    public boolean publicaAnuncio(){
        return m_oRegistoAnuncios.publicaAnuncio(this.m_oAnuncio);
    }

    public String getAnuncioString() {return this.m_oAnuncio.toString();
    }

    public List<Tarefa> getLstTarefasByMailColab() {return m_lstTarefasByMailColab;}

    public RegistoRegimes getRegistoRegimes() { return m_oRegistoRegimes;}

}
