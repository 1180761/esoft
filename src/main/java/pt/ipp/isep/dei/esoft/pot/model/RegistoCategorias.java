package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RegistoCategorias {

    private final Set<Categoria> m_lstCategorias;

    public RegistoCategorias(){
        m_lstCategorias = new HashSet<>();
    }

    public Categoria novaCategoria(String strDesc, AreaAtividade oArea) {
        int id = m_lstCategorias.size() + 1;
        return new Categoria(id, strDesc, oArea);
    }

    public boolean registaCategoria(Categoria oCategoria) {
        if (this.validaCategoria(oCategoria)) {
            return addCategoria(oCategoria);
        }
        return false;
    }

    private boolean addCategoria(Categoria oCategoria) {
        return m_lstCategorias.add(oCategoria);
    }

    public boolean validaCategoria(Categoria oCategoria) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        //
        return bRet;
    }

    public Set<Categoria> getListaCategorias() {
        return m_lstCategorias;
    }

    public List<Categoria> getCategorias() {
        List<Categoria> lc = new ArrayList<>();
        lc.addAll(this.m_lstCategorias);
        return lc;
    }

    public Categoria getCategoriaById(int idCategoria) {
        for (Categoria cat : this.m_lstCategorias) {
            if (idCategoria == cat.getId()) {
                return cat;
            }
        }
        return null;
    }
}
