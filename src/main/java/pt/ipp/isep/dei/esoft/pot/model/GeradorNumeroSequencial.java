package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

public interface GeradorNumeroSequencial {

    int geraNumSequencial();

    Date getDataAtual();


}
