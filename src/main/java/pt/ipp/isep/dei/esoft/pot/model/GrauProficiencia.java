package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Objects;

public class GrauProficiencia {

    private String designacao;
    private int valor;

    public GrauProficiencia(int valor, String designacao){
        this.valor = valor;
        this.designacao = designacao;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GrauProficiencia that = (GrauProficiencia) o;
        return valor == that.valor &&
                Objects.equals(designacao, that.designacao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designacao, valor);
    }

    @Override
    public String toString() {
        return "GrauProficiencia{" +
                "designacao='" + designacao + '\'' +
                ", valor=" + valor +
                '}';
    }
}
