/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author paulomaio
 */
public class EspecificarCompetenciaTecnicaController
{
    private Plataforma m_oPlataforma;
    private CompetenciaTecnica m_oCompetencia;
    private RegistoAreasAtividade m_oRegistoAreasAtividade;
    private RegistoCompetenciasTecnicas m_oRegistoCompetenciasTecnicas;


    public EspecificarCompetenciaTecnicaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
        this.m_oRegistoAreasAtividade = this.m_oPlataforma.getRegistoAreasAtividade();
        this.m_oRegistoCompetenciasTecnicas = this.m_oPlataforma.getRegistoCompetenciasTecnicas();
    }
    
    public List<AreaAtividade> getAreasAtividade()
    {
        return this.m_oRegistoAreasAtividade.getAreasAtividade();
    }
    
    public boolean novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoDetalhada, String areaAtividadeId)
    {
        try
        {
            AreaAtividade area = this.m_oRegistoAreasAtividade.getAreaAtividadeById(areaAtividadeId);
            this.m_oCompetencia = this.m_oRegistoCompetenciasTecnicas.novaCompetenciaTecnica(strId, strDescricaoBreve,strDescricaoDetalhada,area);
            return this.m_oRegistoCompetenciasTecnicas.validaCompetenciaTecnica(this.m_oCompetencia);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCompetencia = null;
            return false;
        }
    }
   
    
    public boolean registaCompetenciaTecnica()
    {
        return this.m_oRegistoCompetenciasTecnicas.registaCompetenciaTecnica(this.m_oCompetencia);
    }

    public String getCompetenciaTecnicaString()
    {
        return this.m_oCompetencia.toString();
    }

    public boolean addGrauProficiencia(int valor, String designacao){
       return m_oCompetencia.addGrauProficiencia(valor,designacao);
    }
}
