package pt.ipp.isep.dei.esoft.pot.model;

public class TipoRegimento {

    private String designacao;
    private String descricaoRegras;
    private boolean objetivo;
    private boolean obrigatorio;

    public TipoRegimento(String designacao, String descricaoRegras, boolean objetivo) {
        this.designacao = designacao;
        this.descricaoRegras = descricaoRegras;
        this.objetivo = objetivo;
    }

    public String getDesignacao() {
        return designacao;
    }

    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    public String getDescricaoRegras() {
        return descricaoRegras;
    }

    public void setDescricaoRegras(String descricaoRegras) {
        this.descricaoRegras = descricaoRegras;
    }

    public Boolean isObjetivo() {
        return objetivo;
    }

    public void setObjetivo(Boolean objetivo) {
        this.objetivo = objetivo;
    }

    public boolean isObrigatorio() {
        return obrigatorio;
    }

    public void setObrigatorio(boolean obrigatorio) {
        this.obrigatorio = obrigatorio;
    }
}
