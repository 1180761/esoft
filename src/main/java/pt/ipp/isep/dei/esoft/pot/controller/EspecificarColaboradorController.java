package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.logging.Level;
import java.util.logging.Logger;

import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarColaboradorController {

    private Plataforma m_oPlataforma;
    private Colaborador m_oColaborador;
    private Organizacao m_oOrganizacao;
    private RegistoOrganizacoes m_oRegistoOrganizacoes;

    //Construtor verifica se o utilizador atual tem permissão para efetuar a tarefa
    public EspecificarColaboradorController(){
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_GESTOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
        this.m_oRegistoOrganizacoes = m_oPlataforma.getRegistoOrganizacoes();
        this.m_oOrganizacao = m_oRegistoOrganizacoes.getOrganizacao("");
    }

    public boolean novoColaborador(String strNome, String strFuncao, String strTelefone, String strEmail){
        try{
            this.m_oColaborador = Organizacao.novoColaborador(strNome,strFuncao,strTelefone,strEmail);
            return true;
        }catch (RuntimeException ex){
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oColaborador = null;
            return false;
        }
    }

    public boolean registaColaborador(){
        String pswd = m_oRegistoOrganizacoes.gerarPassword();
        return m_oOrganizacao.registaColaborador(m_oColaborador,pswd,this.m_oOrganizacao);
    }

    public String colaboradorToString(){
        return m_oColaborador.toString();
    }

}