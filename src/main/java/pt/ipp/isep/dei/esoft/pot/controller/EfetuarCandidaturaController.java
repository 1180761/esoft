package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.Set;

public class EfetuarCandidaturaController {

    private Plataforma m_oPlataforma;
    private Freelancer m_oFreelancer;
    private RegistoFreelancers rFreelancers;
    private RegistoAnuncios rAnuncios;
    private Categoria m_oCategoria;
    private Anuncio m_oAnuncioS;
    private ListaCandidatura m_oListaCandidatura;
    private Candidatura m_oCandidatura;

    private Set<Anuncio> m_listaAnunciosElegiveis;
    private Set<Anuncio> m_listaAnuncios;
    private Set<CaraterCT> m_listaCaratersObrigatoriosA;
    private Set<CaraterCT> m_listaCaratersObrigatoriosF;

    public EfetuarCandidaturaController(){
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_FREELANCER))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();

        AplicacaoPOT app = AplicacaoPOT.getInstance();
        SessaoUtilizador sessao = app.getSessaoAtual();
        String strEmail = sessao.getEmailUtilizador();

        rFreelancers = m_oPlataforma.getRegistoFreelancers();
        m_oFreelancer = rFreelancers.getFreelancerbyEmail(strEmail);
        rAnuncios = m_oPlataforma.getRegistoAnuncios();

        m_listaAnuncios = rAnuncios.getListaAnuncios();
    }

    public boolean novaCandidatura(double valorTarefa, int numDias, String txtApresentacao, String txtMotivacao){
        m_oCandidatura = m_oListaCandidatura.novaCandidatura(valorTarefa,numDias,txtApresentacao,txtMotivacao);
        return true;
    }

    public boolean novaCandidatura(double valorTarefa, int numDias){
        m_oCandidatura = m_oListaCandidatura.novaCandidatura(valorTarefa,numDias);
        return true;
    }

    public boolean registaCandidatura(){
        if(validaCandidatura(m_oCandidatura)){
            m_oListaCandidatura.addCandidatura(m_oCandidatura);
            return true;
        }
        return false;
    }

    public boolean validaCandidatura(Candidatura candidatura){
        return candidatura != null;
    }

    public void selectAnuncio(Anuncio anuncioS){
        for(Anuncio a : m_listaAnunciosElegiveis){
            if (a == anuncioS) {
                m_oAnuncioS = anuncioS;
                m_oListaCandidatura = m_oAnuncioS.getM_listaCanidatura();
                break;
            }
        }
    }

    public Set<Anuncio> getListaAnunciosElegiveis(){
        for(Anuncio a : m_listaAnuncios) {
            m_oCategoria = a.getTarefa().getCategoria();
            m_listaCaratersObrigatoriosA.addAll(m_oCategoria.getCaratersObrigatorias());
            m_listaCaratersObrigatoriosF.addAll(m_oFreelancer.getCaratersObrigatorios());

            if(a.verificaCompetencias(m_listaCaratersObrigatoriosA,m_listaCaratersObrigatoriosF)){
                m_listaAnunciosElegiveis.add(a);
            }
        }
        return m_listaAnunciosElegiveis;
    }

    public String candidaturaToString(){
        return m_oCandidatura.toString();
    }
}
