package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.pot.controller.AplicacaoPOT;

import java.util.Date;
import java.util.List;

import java.util.Set;
import java.util.TimerTask;

public class SeriarCandidaturaTask extends TimerTask {

    private Set<Anuncio> m_lstAnunciosAutomaticosPorSeriar;
    private Set<Candidatura> m_lstCandidaturas;
    private ProcessoAtribuicaoAdjudicacao m_oProcessoAtribuicaoAdjudicacao;

    public SeriarCandidaturaTask(Set<Anuncio> lstAnunciosAutomaticosPorSeriar){
        this.m_lstAnunciosAutomaticosPorSeriar = lstAnunciosAutomaticosPorSeriar;
    }

    @Override
    public void run() {
        seriarCandidaturasSubmetidas();
        atribuirCandidaturasSubmetidas();
    }

    public void seriarCandidaturasSubmetidas(){
        for(Anuncio a : m_lstAnunciosAutomaticosPorSeriar){
            ProcessoSeriacao ps = a.novoProcessoSeriacao();
            ListaCandidatura listaC = a.getM_listaCanidatura();
            m_lstCandidaturas = listaC.getM_listaCandidaturas();

            for(Candidatura cand: m_lstCandidaturas){
                    int ordem = ps.getOrdemByCandidatura(cand);
                    ps.addCandidaturaClassificada(cand,ordem);
            }
            ps.sortListaCandidaturas();
            a.registaProcessoSeriacao(ps);
            a.validaProcessoSeriacao(ps);
            a.setSeriado(true);          //Este set previne que na proxima seriação estes anuncios entrem na lista dos nao seriados.
        }
    }

    public void atribuirCandidaturasSubmetidas(){

        AplicacaoPOT app =  AplicacaoPOT.getInstance();
        Plataforma plataforma = app.getPlataforma();

        RegistoOrganizacoes rOrgs = plataforma.getRegistoOrganizacoes();

        for(Candidatura cand : m_lstCandidaturas){

            Anuncio anuncio = cand.getM_oAnuncio();
            TipoRegimento tipoRegime = anuncio.getTipoRegime();
            boolean obrigatorio = tipoRegime.isObrigatorio();
            Candidatura candidaturaAtribuir = m_lstCandidaturas.iterator().next();
            Freelancer freelancer = candidaturaAtribuir.getM_oFreelancer();
            Tarefa tarefa = anuncio.getTarefa();
            Organizacao org = rOrgs.getOrgByTarefa(tarefa);
            String descTarefa = tarefa.getDescInformal();
            String refAnuncio = tarefa.getRefUnica();
            double periodoTarefa = tarefa.getDuracao();
            double valorAceite = candidaturaAtribuir.getValorTarefa();
            Date dataAtual = plataforma.getDataAtual();
            int nSeqAno = plataforma.geraNumSequencial();

            if(obrigatorio){
                m_oProcessoAtribuicaoAdjudicacao = new ProcessoAtribuicaoAdjudicacao(org,freelancer,descTarefa,periodoTarefa,valorAceite,refAnuncio,nSeqAno,dataAtual);
            }
        }
    }

    public Set<Candidatura> getM_lstCandidaturasOrdenadas(){
        return this.m_lstCandidaturas;
    }

    public Set<Anuncio> getM_lstAnunciosAutomaticosPorSeriar() {
        return m_lstAnunciosAutomaticosPorSeriar;
    }
    public void setM_lstAnunciosAutomaticosPorSeriar(Set<Anuncio> m_lstAnunciosAutomaticosPorSeriar) {
        this.m_lstAnunciosAutomaticosPorSeriar = m_lstAnunciosAutomaticosPorSeriar;
    }

    public ProcessoAtribuicaoAdjudicacao getM_oProcessoAtribuicaoAdjudicacao() {
        return m_oProcessoAtribuicaoAdjudicacao;
    }

    public void setM_oProcessoAtribuicaoAdjudicacao(ProcessoAtribuicaoAdjudicacao m_oProcessoAtribuicaoAdjudicacao) {
        this.m_oProcessoAtribuicaoAdjudicacao = m_oProcessoAtribuicaoAdjudicacao;
    }
}
