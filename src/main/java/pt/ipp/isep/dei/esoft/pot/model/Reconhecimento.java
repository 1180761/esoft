package pt.ipp.isep.dei.esoft.pot.model;

public class Reconhecimento {

    private String designacao;
    private String data;

    private GrauProficiencia m_oGrauproficiencia;
    private CompetenciaTecnica m_oCompetenciaTecnica;

    public Reconhecimento(String designacao, String data){

        this.designacao = designacao;
        this.data = data;
    }

    public CompetenciaTecnica getCompetenciaTecnica(){
        return this.m_oCompetenciaTecnica;
    }

    public int getGrauProficiencia(){
        return m_oGrauproficiencia.getValor();
    }

    //getters and setters
    public String getDesignacao() {
        return designacao;
    }
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
}
