package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jorgi
 */

public class EspecificarCategoriaController {

    private Plataforma m_oPlataforma;
    private Categoria m_oCategoria;
    private RegistoCategorias m_oRegistoCategorias;
    private RegistoAreasAtividade m_oRegistoAreasAtividade;
    private RegistoCompetenciasTecnicas m_oRegistoCompetenciasTecnicas;

    public EspecificarCategoriaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public boolean novaCategoria(String desc, String areaAtividadeId)
    {
        try
        {
            this.m_oRegistoCategorias = this.m_oPlataforma.getRegistoCategorias();
            this.m_oRegistoAreasAtividade = this.m_oPlataforma.getRegistoAreasAtividade();
            this.m_oRegistoCompetenciasTecnicas = this.m_oPlataforma.getRegistoCompetenciasTecnicas();
            AreaAtividade area = this.m_oRegistoAreasAtividade.getAreaAtividadeById(areaAtividadeId);
            this.m_oCategoria = this.m_oRegistoCategorias.novaCategoria(desc, area);
            return this.m_oRegistoCategorias.validaCategoria(this.m_oCategoria);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCategoria = null;
            return false;
        }
    }

    public boolean addCaraterCT(boolean obrigatorio, String idCT, int valor, String designacao){
        CompetenciaTecnica competenciaTecnica = this.m_oRegistoCompetenciasTecnicas.getCompetenciaTecnicaById(idCT);
        GrauProficiencia grauProficiencia = competenciaTecnica.getGrauByValueAndDesig(valor, designacao);
        return this.m_oCategoria.addCaraterCTS(obrigatorio,competenciaTecnica,grauProficiencia);
    }

    public List<AreaAtividade> getAreasAtividade()
    {
        return this.m_oRegistoAreasAtividade.getAreasAtividade();
    }

    public List<CompetenciaTecnica> getCompetenciasTecnicas()
    {
        return this.m_oRegistoCompetenciasTecnicas.getCompetenciasTecnicas();
    }

    public boolean registaCategoria()
    {
        return this.m_oRegistoCategorias.registaCategoria(this.m_oCategoria);
    }

    public String getCategoriaString()
    {
        return this.m_oCategoria.toString();
    }

}
