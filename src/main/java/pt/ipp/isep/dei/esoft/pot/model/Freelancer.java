package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Freelancer {

    private String strNome;
    private String strFuncao;
    private String strEmail;
    private String strTelefone;

    private final Set<Reconhecimento> m_listaReconhecimentos;
    private Set<CaraterCT> m_listaCaraters;
    private final Set<CaraterCT> m_listaCaratersObrigatoriosF;
    private Reconhecimento m_oReconhecimento;

    public Freelancer(String strNome, String strFuncao, String strEmail, String strTelefone){
        if ( (strNome == null) || (strFuncao == null) || (strTelefone == null) || (strEmail == null) ||
                (strNome.isEmpty())|| (strFuncao.isEmpty())|| (strTelefone.isEmpty())|| (strEmail.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.strNome = strNome;
        this.strFuncao = strFuncao;
        this.strEmail = strEmail;
        this.strTelefone = strTelefone;
        this.m_listaReconhecimentos = new HashSet<>();
        this.m_listaCaratersObrigatoriosF = new HashSet<>();
        this.m_listaCaraters = new HashSet<>();
    }

    public Reconhecimento getReconhecimento(){
        for(Reconhecimento r : m_listaReconhecimentos){
            return r;
        }
        return null;
    }

    public CompetenciaTecnica getCompTec(){
        for(Reconhecimento c : m_listaReconhecimentos){
        return c.getCompetenciaTecnica();}
        return null;
    }

    public int getGrauRec(){
        for(Reconhecimento r : m_listaReconhecimentos){
        return r.getGrauProficiencia();}
        return 0;
    }

    //getters and setters
    public String getStrNome() {
        return strNome;
    }
    public void setStrNome(String strNome) {
        this.strNome = strNome;
    }
    public String getStrFuncao() {
        return strFuncao;
    }
    public void setStrFuncao(String strFuncao) {
        this.strFuncao = strFuncao;
    }
    public String getStrEmail() {
        return strEmail;
    }
    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }
    public String getStrTelefone() {
        return strTelefone;
    }
    public void setStrTelefone(String strTelefone) {
        this.strTelefone = strTelefone;
    }

    public Set<Reconhecimento> getM_listaReconhecimentos() {
        return m_listaReconhecimentos;
    }

    public Set<CaraterCT> getCaratersObrigatorios(){
        for(CaraterCT r : m_listaCaraters){
            if(r.isObrigatorio())
            m_listaCaratersObrigatoriosF.add(r);
        }
        return m_listaCaratersObrigatoriosF;
    }

    public Set<CaraterCT> getM_listaCaratersObrigatoriosF() {
        return m_listaCaratersObrigatoriosF;
    }
    public Set<CaraterCT> getM_listaCaraters() {
        return m_listaCaraters;
    }

    @Override
    public String toString() {
        return "Freelancer{" +
                "strNome='" + strNome + '\'' +
                ", strFuncao='" + strFuncao + '\'' +
                ", strEmail='" + strEmail + '\'' +
                ", strTelefone='" + strTelefone + '\'' +
                ", m_listaReconhecimentos=" + m_listaReconhecimentos +
                ", m_listaCaraters=" + m_listaCaraters +
                ", m_listaCaratersObrigatoriosF=" + m_listaCaratersObrigatoriosF +
                ", m_oReconhecimento=" + m_oReconhecimento +
                '}';
    }
}
