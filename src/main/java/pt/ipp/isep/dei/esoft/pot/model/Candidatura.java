package pt.ipp.isep.dei.esoft.pot.model;

public class Candidatura {

    private double valorTarefa;
    private int numDias;
    private String txtApresentacao;
    private String txtMotivacao;

    private Freelancer m_oFreelancer;
    private Anuncio m_oAnuncio;

    public Candidatura(Freelancer freelancer, double valorTarefa, int numDias, Anuncio anuncio){
        if(valorTarefa > 0 && numDias > 0){
            this.valorTarefa = valorTarefa;
            this.numDias = numDias;
            this.m_oFreelancer = freelancer;
            this.m_oAnuncio = anuncio;
        }
    }

    public Candidatura(Freelancer freelancer,double valorTarefa, int numDias, String txtApresentacao, String txtMotivacao, Anuncio anuncio){
        if(valorTarefa > 0 && numDias > 0){
            this.valorTarefa = valorTarefa;
            this.numDias = numDias;
            this.txtApresentacao = txtApresentacao;
            this.txtMotivacao = txtMotivacao;
            this.m_oFreelancer = freelancer;
            this.m_oAnuncio = anuncio;
        }
    }

    //getters and setters
    public double getValorTarefa() {
        return valorTarefa;
    }
    public void setValorTarefa(double valorTarefa) {
        this.valorTarefa = valorTarefa;
    }
    public int getNumDias() {
        return numDias;
    }
    public void setNumDias(int numDias) {
        this.numDias = numDias;
    }
    public String getTxtApresentacao() {
        return txtApresentacao;
    }
    public void setTxtApresentacao(String txtApresentacao) {
        this.txtApresentacao = txtApresentacao;
    }
    public String getTxtMotivacao() {
        return txtMotivacao;
    }
    public void setTxtMotivacao(String txtMotivacao) {
        this.txtMotivacao = txtMotivacao;
    }
    public String toString(){
        return "Custo" + valorTarefa + "\n Duracao: "+numDias + "  \nTexto apresentacao: " +txtApresentacao + "\nTexto motivacao: "+txtMotivacao;
    }
    public Freelancer getM_oFreelancer() {
        return m_oFreelancer;
    }
    public void setM_oFreelancer(Freelancer m_oFreelancer) {
        this.m_oFreelancer = m_oFreelancer;
    }
    public Anuncio getM_oAnuncio() {
        return m_oAnuncio;
    }
}
