package pt.ipp.isep.dei.esoft.pot.model;

public class Classificacao {

    private Candidatura candidatura;
    private int ordem;
    private String justificacao;


    public Classificacao(Candidatura candidatura, int classificacao, String justificacao) {
        this.candidatura = candidatura;
        this.ordem = classificacao;
        this.justificacao = justificacao;
    }

    public Classificacao(Candidatura candidatura, int classificacao) {
        this.candidatura = candidatura;
        this.ordem = classificacao;
    }


    public Candidatura getCandidatura() {
        return candidatura;
    }

    public void setCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }

    public int getClassificacao() {
        return ordem;
    }

    public void setClassificacao(int ordem) {
        this.ordem = ordem;
    }

    public String getJustificacao() {
        return justificacao;
    }

    public void setJustificacao(String justificacao) {
        this.justificacao = justificacao;
    }
}
