package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author jorgi
 */

public class Categoria {

    private int id;
    private String desc;
    private AreaAtividade areaAtividade;
    private Set<CaraterCT> m_listaCaraters;
    private final Set<CaraterCT> m_listaCaraterObrigatorios;

    public Categoria(int id, String desc, AreaAtividade areaAtividade) {
        if ((desc == null) || (areaAtividade == null) || (m_listaCaraters == null) ||
                (desc.isEmpty()) || (m_listaCaraters.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.id = id;
        this.desc = desc;
        this.areaAtividade = areaAtividade;
        this.m_listaCaraters = new HashSet<>();
        this.m_listaCaraterObrigatorios = new HashSet<>();
    }

    public boolean addCaraterCTS(CaraterCT ct){
        return this.m_listaCaraters.add(ct);
    }

    public boolean addCaraterCTS(boolean obrigatorio, CompetenciaTecnica competenciaTecnica, GrauProficiencia grauProficiencia){

        return this.m_listaCaraters.add(new CaraterCT(obrigatorio, grauProficiencia, competenciaTecnica));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public AreaAtividade getAreaAtividade() {
        return areaAtividade;
    }

    public void setAreaAtividade(AreaAtividade areaAtividade) {
        this.areaAtividade = areaAtividade;
    }

    public Set<CaraterCT> getM_listaCaraters() {
        return m_listaCaraters;
    }

    public void setCompetenciasTecnicas(Set<CaraterCT> caraterCTS) {
        this.m_listaCaraters = caraterCTS;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return  id == categoria.id &&
                desc.equals(categoria.desc) &&
                areaAtividade.equals(categoria.areaAtividade) &&
                m_listaCaraters.equals(categoria.m_listaCaraters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, desc, areaAtividade, m_listaCaraters);
    }

    @Override
    public String toString() {
        return "Categoria{" +
                "id = '" + id + '\'' +
                ", descrição = '" + desc + '\'' +
                ", Area de Atividade = " + areaAtividade +
                ", CaraterCTS = " + m_listaCaraters +
                '}';
    }

    public Set<CaraterCT> getCaratersObrigatorias(){
        for (CaraterCT ct : m_listaCaraters){
            if(ct.isObrigatorio()){
                m_listaCaraterObrigatorios.add(ct);
            }
        }
        return m_listaCaraterObrigatorios;
    }

}
