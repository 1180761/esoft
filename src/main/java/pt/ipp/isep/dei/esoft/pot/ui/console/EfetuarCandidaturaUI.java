package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.EfetuarCandidaturaController;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;
import sun.nio.ch.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class EfetuarCandidaturaUI {

    private EfetuarCandidaturaController m_controller;

    public EfetuarCandidaturaUI(){
        m_controller = new EfetuarCandidaturaController();
    }

    public void run(){
        System.out.println("\nEfetuar candidatura:");

        if (introduzDados()) {

            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCandidatura()) {
                    System.out.println("Candidatura efetuada com sucesso!");
                } else {
                    System.out.println("Não possui os requisitos minimos para se candidatar.");
                }
            }
        }else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    public boolean introduzDados(){
        double valorTarefa = 0;
        int numDias = 0;

        Set<Anuncio> listaAnunciosElegiveis = m_controller.getListaAnunciosElegiveis();
        Anuncio anuncio = (Anuncio) Utils.apresentaESeleciona((List)listaAnunciosElegiveis,"Selecione o anuncio ao qual se pretende candidatar:");

            if(anuncio!=null) {
                m_controller.selectAnuncio(anuncio);
                valorTarefa = Utils.readDoubleFromConsole("Valor que deseja receber pela tarefa:");
                numDias = Utils.readIntegerFromConsole("Estima do numero de dias necessários para completar a tarefa:");
            }

                int op;
                List<String> options = new ArrayList<String>();
                options.add("Continuar sem adicionar.");
                options.add("Adicionar texto apresentacao/motivacao");
                op = Utils.apresentaESelecionaIndex(options, "");
                switch (op) {
                    case 0:
                        return m_controller.novaCandidatura(valorTarefa, numDias);
                    case 1:
                        String txtApresentacao = Utils.readLineFromConsole("Texto de apresentacao: ");
                        String txtMotivacao = Utils.readLineFromConsole("Texto de motivacao: ");
                        return m_controller.novaCandidatura(valorTarefa, numDias, txtApresentacao, txtMotivacao);
                }
                return true;
        }

        public void apresentaDados(){
            System.out.println("Dados da candidatura: "+m_controller.candidaturaToString());
        }
}
