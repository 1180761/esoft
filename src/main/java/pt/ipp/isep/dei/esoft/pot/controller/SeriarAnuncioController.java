package pt.ipp.isep.dei.esoft.pot.controller;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.*;

import java.util.Date;
import java.util.List;

public class SeriarAnuncioController {

    private Plataforma m_oPlataforma;
    private SessaoUtilizador sessaoUtilizador;
    private String email;
    private Organizacao organizacao;
    private RegistoOrganizacoes registoOrganizacoes;
    private RegistoAnuncios registoAnuncios;
    private Anuncio anuncio;
    private Colaborador colaborador;
    private ListaCandidatura listaCandidatura;
    private ProcessoSeriacao processoSeriacao;
    private Candidatura candidatura;
    private ProcessoAtribuicaoAdjudicacao processoAtribuicaoAdjudicacao;

    public SeriarAnuncioController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
        this.sessaoUtilizador = AplicacaoPOT.getInstance().getSessaoAtual();
        this.email = this.sessaoUtilizador.getEmailUtilizador();
        this.registoOrganizacoes = this.m_oPlataforma.getRegistoOrganizacoes();
        this.organizacao = this.registoOrganizacoes.getOrganizacaoByEmailColab(this.email);
        this.colaborador = organizacao.getColaboradorByEmail(email);
        this.registoAnuncios = this.m_oPlataforma.getRegistoAnuncios();
    }

    public List<Anuncio> getAnunciosPorSeriarNaoAutomaticos(){
        return this.registoAnuncios.getAnunciosPorSeriarNaoAutomaticos(colaborador);
    }

    public List<Candidatura> getCandidaturas(String anuncioTarefaId){
        anuncio = registoAnuncios.getAnuncioByIdTarefa(anuncioTarefaId);
        listaCandidatura = anuncio.getM_listaCanidatura();
        processoSeriacao = anuncio.novoProcessoSeriacao(colaborador);
        return (List) listaCandidatura.getM_listaCandidaturas();
    }

    public boolean classificarCandidatura(Candidatura candidatura, int classificacao, String justificacao){
        return processoSeriacao.addCandidaturaClassificada(candidatura,classificacao, justificacao);
    }

    public List<Colaborador> getColaboradores(){
        return (List) organizacao.getM_lstColaboradores();
    }

    public boolean addColaboradorParticipante(String email){
        colaborador = organizacao.getColaboradorByEmail(email);
        return processoSeriacao.addColaborador(colaborador);
    }

    public void addTextoConclusao(String conclusao){
        processoSeriacao.setConclusao(conclusao);
        processoSeriacao.sortListaCandidaturas();
    }

    public ProcessoAtribuicaoAdjudicacao novaAdjudicacao(){
        Tarefa tarefa = anuncio.getTarefa();
        String descTarefa = tarefa.getDesignacao();
        String refAnuncio = tarefa.getRefUnica();
        double pTarefa = tarefa.getDuracao();
        double vAceite = candidatura.getValorTarefa();
        int nSeqAno = m_oPlataforma.geraNumSequencial();
        Date dAdjud = m_oPlataforma.getDataAtual();
        processoAtribuicaoAdjudicacao = anuncio.novoProcessoAtribuicao(organizacao,candidatura.getM_oFreelancer(),descTarefa,pTarefa,vAceite,refAnuncio,nSeqAno, dAdjud);
        return processoAtribuicaoAdjudicacao;
    }

    public boolean AdjudicaAnuncio(){
        return anuncio.registaProcessoAtribuicao(processoAtribuicaoAdjudicacao);
    }

    public String getMelhorCandidato(){
        candidatura = processoSeriacao.getClassificacoes().iterator().next().getCandidatura();
        return candidatura.getM_oFreelancer().toString();
    }

    public boolean registaSeriacao(){
        return anuncio.registaProcessoSeriacao(processoSeriacao);
    }

    public String getSeriarAnuncioString()
    {
        return this.processoSeriacao.toString();
    }
}
