package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.ProcessoAtribuicaoAdjudicacaoController;
import pt.ipp.isep.dei.esoft.pot.controller.SeriarAnuncioController;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProcessoAtribuicaoAdjudicacaoUI {

    private ProcessoAtribuicaoAdjudicacaoController m_controller;
    public ProcessoAtribuicaoAdjudicacaoUI()
    {
        m_controller = new ProcessoAtribuicaoAdjudicacaoController();
    }

    public void run()
    {
        System.out.println("\nSeriar Anuncio:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.AdjudicaAnuncio()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {

        List<Anuncio> listAnuncios = m_controller.getAnunciosSeriados();

        String anuncioTarefaId = "";
        Anuncio anuncio = (Anuncio)Utils.apresentaESeleciona(listAnuncios, "Selecione o anuncio pretendido:");
        if (anuncio != null)
            anuncioTarefaId = anuncio.getTarefa().getRefUnica();

        System.out.println(m_controller.getMelhorCandidato(anuncio).toString());

        m_controller.novaAdjudicacao();

        return true;
    }

    private void apresentaDados()
    {
        System.out.println("\nSeriacao do Anuncio:\n" + m_controller.getProcessoAtribuicaoAdjudicacaoString());
    }
}
