package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class ListaTarefas {

    private final List<Tarefa> m_lstTarefas;
    private final List<Tarefa> m_lstTarefasByMailColab;

    public ListaTarefas(){ m_lstTarefas = new ArrayList<>();
    m_lstTarefasByMailColab = new ArrayList<>();}

    public Tarefa novaTarefa(String refUnica, String designacao, String descInformal, String descCaracterTecnico, double duracao, double custo, Categoria categoria, String mailColab) {
        int id = m_lstTarefas.size() + 1;
        return new Tarefa(refUnica, designacao, descInformal, descCaracterTecnico, duracao, custo, categoria, mailColab);
    }

    public boolean validarNovaTarefa(Tarefa tar) {
        for (Tarefa tarefa : m_lstTarefas) {
            if (tarefa.getRefUnica().equals(tar.getRefUnica())) {
                return false;
            }
        }
        return true;
    }

    private boolean addTarefa(Tarefa tarefa) {
        return m_lstTarefas.add(tarefa);
    }

    public boolean especificaTarefa(Tarefa tar) {
        if (validarNovaTarefa(tar)) {
            addTarefa(tar);
        }
        return false;
    }

    public List<Tarefa> getTarefas() {
        List<Tarefa> lt = new ArrayList<>();
        lt.addAll(this.m_lstTarefas);
        return lt;
    }

    public Tarefa getTarefaByRefUnica(Tarefa refTar) {
        for (Tarefa tar : this.m_lstTarefas) {
            if (refTar.getRefUnica() == tar.getRefUnica()) {
                return refTar;
            }
        }

        return null;
    }

    public Tarefa getTarefaByMailColabByRefUnica(String refTar) {
        for (Tarefa tar : this.m_lstTarefasByMailColab) {
            if (refTar == tar.getRefUnica()) {
                return tar;
            }
        }

        return null;
    }
    public List<Tarefa> getListaTarefasByMailColab(String mailColab) {
        for (Tarefa ltbmc : this.m_lstTarefas) {
            if (mailColab == ltbmc.getMailColab()) {
                m_lstTarefasByMailColab.add(ltbmc);
            }
        }
        return m_lstTarefasByMailColab;
    }
}
