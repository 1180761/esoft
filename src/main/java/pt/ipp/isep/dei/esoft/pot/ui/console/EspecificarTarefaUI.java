package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.EspecificarTarefaController;
import pt.ipp.isep.dei.esoft.pot.model.Categoria;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.List;

/**
 *
 * @author jorgi
 */

public class EspecificarTarefaUI {

    private EspecificarTarefaController m_controller;
    public EspecificarTarefaUI()
    {
        m_controller = new EspecificarTarefaController();
    }

    public void run()
    {
        System.out.println("\nEspecificar Tarefa:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.especificaTarefa()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        String refUnica = Utils.readLineFromConsole("Referência Única: ");
        String designacao = Utils.readLineFromConsole("Designação: ");
        String descInformal = Utils.readLineFromConsole("Descrição Informal: ");
        String descCaracterTecnico = Utils.readLineFromConsole("Descrição de Carácter Técnico: ");
        double duracao = Utils.readDoubleFromConsole("Duração: ");
        double custo = Utils.readDoubleFromConsole("Custo: ");

        List<Categoria> categorias = m_controller.getListaCategorias();

        int catId = 0;
        Categoria cat = (Categoria)Utils.apresentaESeleciona(categorias, "Selecione a Categoria a que é referente esta Tarefa:");
        if (cat != null)
            catId = cat.getId();

        return m_controller.novaTarefa(refUnica, designacao, descInformal, descCaracterTecnico, duracao, custo, catId);
    }

    private void apresentaDados()
    {
        System.out.println("\nTarefa:\n" + m_controller.getTarefaString());
    }
}