package pt.ipp.isep.dei.esoft.pot.ui.console;

import      pt.ipp.isep.dei.esoft.pot.controller.EspecificarCategoriaController;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.Categoria;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jorgi
 */

public class EspecificarCategoriaUI {

    private EspecificarCategoriaController m_controller;
    public EspecificarCategoriaUI()
    {
        m_controller = new EspecificarCategoriaController();
    }

    public void run()
    {
        System.out.println("\nEspecificar Categoria:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCategoria()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        String strId = Utils.readLineFromConsole("Descrição: ");

        List<AreaAtividade> listAreas = m_controller.getAreasAtividade();
        List<CompetenciaTecnica> listCompetencias = m_controller.getCompetenciasTecnicas();

        String areaId = "";
        AreaAtividade area = (AreaAtividade)Utils.apresentaESeleciona(listAreas, "Selecione a Área de Atividade a que é referente esta Categoria:");
        if (area != null)
            areaId = area.getCodigo();

        m_controller.novaCategoria(strId, areaId);

        String ctId = "";
        int op;
        List<String> options = new ArrayList<String>();
        options.add("Adicionar outra competencia tecnica");
        options.add("Continuar com registo da categoria");
        do {
            CompetenciaTecnica competenciaTecnica = (CompetenciaTecnica) Utils.apresentaESeleciona(listCompetencias, "Selecione uma competencia técnica requerida:");
            if (competenciaTecnica != null){
                ctId = competenciaTecnica.getM_strId();
                List<GrauProficiencia> grauProficienciaList = competenciaTecnica.getM_oListGrauProficiencia();
                GrauProficiencia grauProficiencia = (GrauProficiencia) Utils.apresentaESeleciona(grauProficienciaList, "Selecione o grau de proficiencia pretendido:");
                boolean ob = false;
                String obrigatorio = Utils.readLineFromConsole("Obrigatoriedade: ");
                if(obrigatorio.equalsIgnoreCase("Obrigatorio")){
                    ob = true;
                }
                m_controller.addCaraterCT(ob, ctId, grauProficiencia.getValor(), grauProficiencia.getDesignacao());
                listCompetencias.remove(competenciaTecnica);
            }
            op = Utils.apresentaESelecionaIndex(options, "");
        } while(op != 1);

        return m_controller.novaCategoria(strId, areaId);
    }

    private void apresentaDados()
    {
        System.out.println("\nCategoria:\n" + m_controller.getCategoriaString());
    }
}
