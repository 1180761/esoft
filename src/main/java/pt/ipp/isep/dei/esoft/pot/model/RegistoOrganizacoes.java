package pt.ipp.isep.dei.esoft.pot.model;

import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RegistoOrganizacoes {

    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<Organizacao> m_lstOrganizacoes;

    public RegistoOrganizacoes(AutorizacaoFacade autorizacaoFacade){
        m_lstOrganizacoes = new HashSet<>();
        m_oAutorizacao = autorizacaoFacade;
    }

    public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite, String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor) {
        return new Organizacao(strNome, strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao, String strPwd) {
        if (this.validaOrganizacao(oOrganizacao, strPwd)) {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor, strEmailGestor, strPwd,
                    new String[]{Constantes.PAPEL_GESTOR_ORGANIZACAO, Constantes.PAPEL_COLABORADOR_ORGANIZACAO})) {
                return addOrganizacao(oOrganizacao);
            }
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao) {
        return m_lstOrganizacoes.add(oOrganizacao);
    }

    public boolean validaOrganizacao(Organizacao oOrganizacao, String strPwd) {
        boolean bRet = true;

        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail())) {
            bRet = false;
        }
        if (strPwd == null) {
            bRet = false;
        }
        if (strPwd.isEmpty()) {
            bRet = false;
        }
        //

        return bRet;
    }

    public Organizacao getOrganizacao(String email) {
        for (Organizacao org : m_lstOrganizacoes) {
            if (org.getGestor().getEmail().equals(m_oAutorizacao.getSessaoAtual().getEmailUtilizador())) {
                return org;
            }
        }
        return null;
    }

    public String gerarPassword(){
        return "";
    }

    public Organizacao getOrganizacaoByEmailColab(String email){
        for(Organizacao org : m_lstOrganizacoes){
            for(Colaborador colaborador : org.getM_lstColaboradores()){
                if(colaborador.getEmail().equals(email)){
                    return org;
                }
            }
        }
        return null;
    }

    public Organizacao getOrgByTarefa(Tarefa tarefa){
        for(Organizacao organizacao : m_lstOrganizacoes){
            ListaTarefas listaTarefas = organizacao.getListaTarefas();
           for(Tarefa tar : listaTarefas.getTarefas()){
               if (tar == tarefa){
                   return organizacao;
               }
           }
        }
        return null;
    }
}
