package pt.ipp.isep.dei.esoft.pot.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class RegistoAnuncios {


    private final Set<Anuncio> m_lstAnuncios;
    private List<Anuncio> m_lstAnunciosAutomaticosPorSeriar;

    public RegistoAnuncios(){ m_lstAnuncios = new HashSet<>(); }

    public Anuncio novoAnuncio(Date pPublicitacao, Date pApresentacao, Date pSeriacao, Date pDecisao, Tarefa tarefa, String mailColab) {
        int id = m_lstAnuncios.size() + 1;
        this.m_lstAnunciosAutomaticosPorSeriar = new ArrayList<>();
        return new Anuncio(pPublicitacao, pApresentacao, pSeriacao, pDecisao, mailColab, tarefa);
    }

    public boolean validarNovoAnuncio(Anuncio anuncio) {
        for (Anuncio an : m_lstAnuncios) {
            if (an.getTarefa().equals(anuncio.getTarefa())) {
                return false;
            }
        }
        return true;
    }

    private boolean addAnuncio(Anuncio anuncio) {
        return m_lstAnuncios.add(anuncio);
    }

    public boolean publicaAnuncio(Anuncio an) {
        if (validarNovoAnuncio(an)) {
            addAnuncio(an);
        }
        return false;
    }

    public List<Anuncio> getAnuncios() {
        List<Anuncio> la = new ArrayList<>();
        la.addAll(this.m_lstAnuncios);
        return la;
    }

    public Anuncio getAnuncioByIdTarefa(Tarefa tar) {
        for (Anuncio an : this.m_lstAnuncios) {
            if (tar == an.getTarefa()) {
                return an;
            }
        }

        return null;
    }

    public Anuncio getAnuncioByIdTarefa(String tar) {
        for (Anuncio an : this.m_lstAnuncios) {
            if (tar == an.getTarefa().getRefUnica()) {
                return an;
            }
        }

        return null;
    }

    public List<Anuncio> getAnunciosByEmail(String email){
        List<Anuncio> list = new ArrayList<>();
        for(Anuncio anuncio : this.m_lstAnuncios){
            if(anuncio.getMailColab().equals(email)){
                list.add(anuncio);
            }
        }
        return list;
    }

    public Set<Anuncio> getListaAnuncios() {
        return m_lstAnuncios;
    }

    public List<Anuncio> getAnunciosPorSeriarNaoAutomaticos(Colaborador colab){
        List<Anuncio> list = new ArrayList<>();
        Date todayDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        for(Anuncio anuncio : this.m_lstAnuncios){
            if(anuncio.getMailColab().equals(colab.getEmail())) {
                if (anuncio.getTipoRegime().getDesignacao().equals("subjetivo") && anuncio.getProcessoSeriacao() == null && todayDate.after(anuncio.getdInicioSeria()) && todayDate.before(anuncio.getdFimSeria())) {
                    list.add(anuncio);
                }
            }
        }
        return list;
    }

    public Set<Anuncio> getM_lstAnunciosAutomaticosPorSeriar(){
        Set<Anuncio> m_lstAnunciosAutomaticosPorSeriar = new HashSet<>();

        for(Anuncio a : m_lstAnuncios){
            TipoRegimento tipoRegime = a.getTipoRegime();
            boolean objetivo = tipoRegime.isObjetivo();
            Date dInicioSeria = a.getdInicioSeria();
            Date dFimSeria = a.getdFimSeria();
            boolean isSeriado = a.isSeriado();
            if(!isSeriado && objetivo ) {    //TODO Comparar datas
                m_lstAnunciosAutomaticosPorSeriar.add(a);
            }
        }
        return m_lstAnunciosAutomaticosPorSeriar;
    }

    public void setM_lstAnunciosAutomaticosPorSeriar(List<Anuncio> m_lstAnunciosAutomaticosPorSeriar) {
        this.m_lstAnunciosAutomaticosPorSeriar = m_lstAnunciosAutomaticosPorSeriar;
    }

    public List<Anuncio> getAnunciosSeriados(){
        List<Anuncio> list = new ArrayList<>();
        for(Anuncio a : m_lstAnuncios){
            if(a.getProcessoSeriacao() != null){
                list.add(a);
            }
        }
        return list;
    }

}

