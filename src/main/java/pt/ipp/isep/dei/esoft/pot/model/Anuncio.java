package pt.ipp.isep.dei.esoft.pot.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class Anuncio {
    private Date dInicioPub;
    private Date dFimPub;
    private Date dInicioCand;
    private Date dFimCand;          //TODO implementar no construtor
    private Date dInicioSeria;      // alterações feitas mediante md do stor.
    private Date dFimSeria;
    private Date pDecisao;
    private Tarefa tarefa;
    private String mailColab;
    private TipoRegimento tipoRegime;
    private boolean isSeriado;
    private ProcessoSeriacao processoSeriacao;
    private ProcessoAtribuicaoAdjudicacao processoAtribuicao;

    private Tarefa m_oTarefa;
    private ListaCandidatura m_listaCanidatura;
    private List<Candidatura> m_lstCandidaturasOrdenadas;


    public Anuncio(Date pPublicitacao, Date pApresentacao, Date pSeriacao, Date pDecisao, String mailColab, Tarefa tarefa ) {

        if ((pPublicitacao == null) || (pApresentacao == null) || (pSeriacao == null) ||  (pDecisao == null) || (tarefa == null))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        this.dInicioPub = pPublicitacao;
        this.dInicioCand = pApresentacao;
        this.dInicioSeria = pSeriacao;
        this.pDecisao = pDecisao;
        this.tarefa = tarefa;
        this.mailColab = mailColab;
        this.m_listaCanidatura = new ListaCandidatura();
    }

    public Date getdInicioPub() {
        return dInicioPub;
    }

    public void setdInicioPub(Date dInicioPub) {
        this.dInicioPub = dInicioPub;
    }

    public Date getdFimPub() {
        return dFimPub;
    }

    public void setdFimPub(Date dFimPub) {
        this.dFimPub = dFimPub;
    }

    public Date getdInicioCand() {
        return dInicioCand;
    }

    public void setdInicioCand(Date dInicioCand) {
        this.dInicioCand = dInicioCand;
    }

    public Date getdFimCand() {
        return dFimCand;
    }

    public void setdFimCand(Date dFimCand) {
        this.dFimCand = dFimCand;
    }

    public Date getdInicioSeria() {
        return dInicioSeria;
    }

    public void setdInicioSeria(Date dInicioSeria) {
        this.dInicioSeria = dInicioSeria;
    }

    public Date getdFimSeria() {
        return dFimSeria;
    }

    public void setdFimSeria(Date dFimSeria) {
        this.dFimSeria = dFimSeria;
    }

    public Date getpDecisao() {
        return pDecisao;
    }

    public void setpDecisao(Date pDecisao) {
        this.pDecisao = pDecisao;
    }

    public Tarefa getTarefa() {
        return tarefa;
    }

    public void setTarefa(Tarefa tarefa) {
        this.tarefa = tarefa;
    }

    public String getMailColab() {
        return mailColab;
    }

    public void setMailColab(String mailColab) {
        this.mailColab = mailColab;
    }

    public TipoRegimento getTipoRegime() {
        return tipoRegime;
    }

    public void setTipoRegime(TipoRegimento tipoRegime) {
        this.tipoRegime = tipoRegime;
    }

    public boolean isSeriado() {
        return isSeriado;
    }

    public void setSeriado(boolean seriado) {
        isSeriado = seriado;
    }

    public ProcessoSeriacao getProcessoSeriacao() {
        return processoSeriacao;
    }

    public void setProcessoSeriacao(ProcessoSeriacao processoSeriacao) {
        this.processoSeriacao = processoSeriacao;
    }

    public void setProcessoAtribuicao(ProcessoAtribuicaoAdjudicacao processoAtribuicao){
        this.processoAtribuicao = processoAtribuicao;
    }

    public Tarefa getM_oTarefa() {
        return m_oTarefa;
    }

    public void setM_oTarefa(Tarefa m_oTarefa) {
        this.m_oTarefa = m_oTarefa;
    }

    public void setM_listaCanidatura(ListaCandidatura m_listaCanidatura) {
        this.m_listaCanidatura = m_listaCanidatura;
    }

    public ListaCandidatura getM_listaCanidatura() {
        return m_listaCanidatura;
    }

    public ProcessoAtribuicaoAdjudicacao getProcessoAtribuicao() {
        return processoAtribuicao;
    }

    public boolean verificaCompetencias(Set<CaraterCT> listaCompetenciasObrigatoriasA, Set<CaraterCT> listacompetenciasObrigatoriasF){
        for (CaraterCT anuncioct : listaCompetenciasObrigatoriasA){
            for(CaraterCT freelancerct : listacompetenciasObrigatoriasF){
                if(anuncioct.getCompetenciaTecnica() == freelancerct.getCompetenciaTecnica()){
                    if(freelancerct.getGrauProficiencia().getValor() >= anuncioct.getGrauProficiencia().getValor()){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public ProcessoSeriacao novoProcessoSeriacao(Colaborador colab){
        return new ProcessoSeriacao(this.tipoRegime, colab);
    }

    public ProcessoSeriacao novoProcessoSeriacao(){
         return new ProcessoSeriacao(tipoRegime);
    }

    public boolean registaProcessoSeriacao(ProcessoSeriacao processoSeriacao){
        processoSeriacao.setData(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        if(validaProcessoSeriacao(processoSeriacao) && processoSeriacao.valida()){
            setProcessoSeriacao(processoSeriacao);
            return true;
        }
        return false;
    }

    public boolean validaProcessoSeriacao(ProcessoSeriacao processoSeriacao){
        return true;
    }

    public ProcessoAtribuicaoAdjudicacao novoProcessoAtribuicao(Organizacao org, Freelancer freelancer, String descTarefa, double periodoTarefa, double valorAceite, String refAnuncio, int nSeqAno, Date dAdju){
        return new ProcessoAtribuicaoAdjudicacao(org, freelancer, descTarefa, periodoTarefa, valorAceite, refAnuncio, nSeqAno, dAdju);
    }

    public boolean registaProcessoAtribuicao(ProcessoAtribuicaoAdjudicacao processoAtribuicao){
        processoAtribuicao.setData(Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        if(validaProcessoAtribuicao(processoAtribuicao) && processoAtribuicao.valida()){
            setProcessoAtribuicao(processoAtribuicao);
            return true;
        }
        return false;
    }

    public boolean validaProcessoAtribuicao(ProcessoAtribuicaoAdjudicacao processoAtribuicao){
        return processoAtribuicao != null;
    }


    public List<Candidatura> getM_lstCandidaturasOrdenadas() {
        return m_lstCandidaturasOrdenadas;
    }

    public void setM_lstCandidaturasOrdenadas(List<Candidatura> m_lstCandidaturasOrdenadas) {
        this.m_lstCandidaturasOrdenadas = m_lstCandidaturasOrdenadas;
    }
}
