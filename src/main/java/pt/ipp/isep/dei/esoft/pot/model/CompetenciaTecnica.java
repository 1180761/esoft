/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.*;

/**
 *
 * @author paulomaio
 */
public class CompetenciaTecnica
{
    private String m_strId;
    private String m_strDescricaoBreve;
    private String m_strDescricaoDetalhada;
    private AreaAtividade m_oAreaAtividade;
    private List<GrauProficiencia> m_oListGrauProficiencia;
    private GrauProficiencia m_oGrauProficiencia;
    
    public CompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoDetalhada, AreaAtividade oArea)
    {
        if ( (strId == null) || (strDescricaoBreve == null) || (strDescricaoDetalhada == null) ||
                (oArea == null) || (strId.isEmpty())|| (strDescricaoBreve.isEmpty()) || (strDescricaoDetalhada.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.m_strId = strId;
        this.m_strDescricaoBreve = strDescricaoBreve;
        this.m_strDescricaoDetalhada = strDescricaoDetalhada;
        m_oAreaAtividade = oArea;
        this.m_oListGrauProficiencia = new ArrayList<>();
    }

    public String getM_strId() {
        return m_strId;
    }

    public void setM_strId(String m_strId) {
        this.m_strId = m_strId;
    }

    public String getM_strDescricaoBreve() {
        return m_strDescricaoBreve;
    }

    public void setM_strDescricaoBreve(String m_strDescricaoBreve) {
        this.m_strDescricaoBreve = m_strDescricaoBreve;
    }

    public String getM_strDescricaoDetalhada() {
        return m_strDescricaoDetalhada;
    }

    public void setM_strDescricaoDetalhada(String m_strDescricaoDetalhada) {
        this.m_strDescricaoDetalhada = m_strDescricaoDetalhada;
    }

    public AreaAtividade getM_oAreaAtividade() {
        return m_oAreaAtividade;
    }

    public void setM_oAreaAtividade(AreaAtividade m_oAreaAtividade) {
        this.m_oAreaAtividade = m_oAreaAtividade;
    }

    public List<GrauProficiencia> getM_oListGrauProficiencia() {
        return m_oListGrauProficiencia;
    }

    public void setM_oListGrauProficiencia(List<GrauProficiencia> m_oListGrauProficiencia) {
        this.m_oListGrauProficiencia = m_oListGrauProficiencia;
    }

    public GrauProficiencia getGrauByValueAndDesig(int valor, String designacao){
        for(GrauProficiencia grau : m_oListGrauProficiencia){
            if(grau.getDesignacao().equals(designacao) && grau.getValor() == valor){
                return grau;
            }
        }
        return null;
    }

    public int getGrauProficiencia(){
        return m_oGrauProficiencia.getValor();
    }

    public boolean hasId(String strId)
    {
        return this.m_strId.equalsIgnoreCase(strId);
    }

    public boolean addGrauProficiencia(int valor, String desig){
        return this.m_oListGrauProficiencia.add(new GrauProficiencia(valor,desig));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompetenciaTecnica that = (CompetenciaTecnica) o;
        return Objects.equals(m_strId, that.m_strId) &&
                Objects.equals(m_strDescricaoBreve, that.m_strDescricaoBreve) &&
                Objects.equals(m_strDescricaoDetalhada, that.m_strDescricaoDetalhada) &&
                Objects.equals(m_oAreaAtividade, that.m_oAreaAtividade) &&
                Objects.equals(m_oListGrauProficiencia, that.m_oListGrauProficiencia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(m_strId, m_strDescricaoBreve, m_strDescricaoDetalhada, m_oAreaAtividade, m_oListGrauProficiencia);
    }

    @Override
    public String toString() {
        return "CompetenciaTecnica{" +
                "m_strId='" + m_strId + '\'' +
                ", m_strDescricaoBreve='" + m_strDescricaoBreve + '\'' +
                ", m_strDescricaoDetalhada='" + m_strDescricaoDetalhada + '\'' +
                ", m_oAreaAtividade=" + m_oAreaAtividade +
                ", m_oListGrauProficiencia=" + m_oListGrauProficiencia +
                '}';
    }
}
