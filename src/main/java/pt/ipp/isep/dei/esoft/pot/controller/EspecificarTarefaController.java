package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;

import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EspecificarTarefaController {

    private Plataforma m_oPlataforma;
    private Organizacao m_oOrganizacao;
    private Tarefa m_oTarefa;
    private RegistoCategorias m_oRegistoCategorias;
    private ListaTarefas m_oListaTarefas;
    private SessaoUtilizador m_oSessaoUtilizador;
    private RegistoOrganizacoes m_oRegistoOrganizacoes;
    private String email;


public EspecificarTarefaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_COLABORADOR_ORGANIZACAO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
        this.m_oSessaoUtilizador = AplicacaoPOT.getInstance().getSessaoAtual();
        this.email = this.m_oSessaoUtilizador.getEmailUtilizador();
        this.m_oRegistoOrganizacoes = this.m_oPlataforma.getRegistoOrganizacoes();
        this.m_oOrganizacao = this.m_oRegistoOrganizacoes.getOrganizacaoByEmailColab(this.email);
    }

    public boolean novaTarefa(String refUnica, String designacao, String descInformal, String descCaracterTecnico, double duracao, double custo, int idCategoria){
        try{
            this.m_oRegistoCategorias = this.m_oPlataforma.getRegistoCategorias();
            this.m_oListaTarefas = this.m_oOrganizacao.getListaTarefas();
            Categoria categoria = setCategorias(idCategoria);
            this.m_oTarefa = m_oListaTarefas.novaTarefa(refUnica, designacao, descInformal, descCaracterTecnico, duracao, custo, categoria, email);
            return m_oListaTarefas.validarNovaTarefa(m_oTarefa);
        }catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oTarefa = null;
            return false;
        }
    }

    public boolean especificaTarefa(){
        return m_oListaTarefas.especificaTarefa(this.m_oTarefa);
    }

    public Categoria setCategorias(int idCategoria)
    {
        return this.m_oRegistoCategorias.getCategoriaById(idCategoria);
    }

    public List<Categoria> getListaCategorias(){
        return m_oRegistoCategorias.getCategorias();
    }
    
    public String getTarefaString()
    {
        return this.m_oTarefa.toString();
    }

}
