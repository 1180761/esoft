package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.EspecificarColaboradorController;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacoes;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

public class EspecificarColaboradorUI {

    private EspecificarColaboradorController m_Controller;

    public EspecificarColaboradorUI(){m_Controller = new EspecificarColaboradorController();}

    public void run(){

        System.out.println("\nEspecificar um novo colaborador:");

        if(introduzDados()){

            apresentaDados();

                if(Utils.confirma("Confirma os dados introduzidos? (S/N)")){
                    if(m_Controller.registaColaborador()){
                        System.out.println("Operação efetuada com sucesso.");
                    }else{
                        System.out.println("Não foi possivel efetuar o registo do colaborador.");
                    }
                }
        }else{
            System.out.println("Ocorreu um erro, operação cancelada.");
        }
    }

    private boolean introduzDados(){
        String strNome = Utils.readLineFromConsole("Nome: ");
        String strFuncao = Utils.readLineFromConsole("Função: ");
        String strTelefone = Utils.readLineFromConsole("Telefone: ");
        String strEmail = Utils.readLineFromConsole("Email: ");

        return m_Controller.novoColaborador(strNome,strFuncao,strTelefone,strEmail);
    }

    private void apresentaDados(){
        System.out.println("\nColaborador: \n"+m_Controller.colaboradorToString());
    }
}