package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.SeriarAnuncioController;
import pt.ipp.isep.dei.esoft.pot.model.*;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SeriarAnuncioUI {

    private SeriarAnuncioController m_controller;
    public SeriarAnuncioUI()
    {
        m_controller = new SeriarAnuncioController();
    }

    public void run()
    {
        System.out.println("\nSeriar Anuncio:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaSeriacao()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }

    private boolean introduzDados() {
        String strId = Utils.readLineFromConsole("Descrição: ");

        List<Anuncio> listAnuncios = m_controller.getAnunciosPorSeriarNaoAutomaticos();

        String anuncioTarefaId = "";
        Anuncio anuncio = (Anuncio)Utils.apresentaESeleciona(listAnuncios, "Selecione o anuncio pretendido:");
        if (anuncio != null)
            anuncioTarefaId = anuncio.getTarefa().getRefUnica();

        List<Candidatura> listCandidaturas = m_controller.getCandidaturas(anuncioTarefaId);

        int ordem;
        String justificacao;
        do {
            Candidatura candidatura = (Candidatura) Utils.apresentaESeleciona(listCandidaturas, "Selecione uma candidatura para classificar:");
            if (candidatura != null){
                ordem = Utils.readIntegerFromConsole("Ordem: ");
                justificacao = Utils.readLineFromConsole("Justificação: ");
                m_controller.classificarCandidatura(candidatura,ordem, justificacao);
                listCandidaturas.remove(candidatura);
            }
        } while(listCandidaturas.isEmpty() != false);

        List<Colaborador> listColaboradores = m_controller.getColaboradores();
        int op;
        String email;
        List<String> options = new ArrayList<String>();
        options.add("Adicionar um participante");
        options.add("Continuar com registo");
        op = Utils.apresentaESelecionaIndex(options, "");
        while(op != 1) {
            Colaborador colaborador = (Colaborador) Utils.apresentaESeleciona(listColaboradores, "Selecione um colaborador que tenha participada na seriação:");
            if (colaborador != null){
                email = colaborador.getEmail();
                m_controller.addColaboradorParticipante(email);
                listColaboradores.remove(colaborador);
            }
            op = Utils.apresentaESelecionaIndex(options, "");
        };

        String conclusao = Utils.readLineFromConsole("Conclusão: ");
        m_controller.addTextoConclusao(conclusao);
        options.clear();
        options.add("Atribuir tarefa");
        options.add("Continuar");
        op = Utils.apresentaESelecionaIndex(options, "");
        if(op == 0){
            System.out.println(m_controller.getMelhorCandidato());
            System.out.println(m_controller.novaAdjudicacao().toString());
            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.AdjudicaAnuncio()) {
                    System.out.println("Atribuição efetuada com sucesso.");
                } else {
                    System.out.println("Não foi possivel a atribuição com sucesso.");
                }
            }
        }

        return true;
    }

    private void apresentaDados()
    {
        System.out.println("\nSeriacao do Anuncio:\n" + m_controller.getSeriarAnuncioString());
    }


}
